<?php

namespace App\Http\Controllers;

use App\Helpers\Exception;
use App\Helpers\Exception\videoDownloaderException;
use App\Helpers\VideoDownloader;
use App\Log;
use App\Video;
use Crawler;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Throwable;

class VideoController extends Controller
{
    const WATCH_URL_REGEX = '#(?:/play:|vid=)([0-9a-f]{10}|[0-9a-f]{8})(?:[^0-9a-f]|$)#';
    const WATCH_YOUTUBE_URL_REGEX = '#/play:m([0-9a-f]{10}|[0-9a-f]{8})(?:[^0-9a-f]|$)#';
    const VIDEOS_PER_PAGE = 100;

    public function index(Request $request)
    {
        $vars = [
            'description' => 'Сваляй директно от vbox7 на MP3. Бързо, лесно и без допълнителни програми. Можете да свалите клипа или само песента като MP3.',
        ];

        if ($request->has('error')) {
            $vars['error'] = VideoDownloader::getErrorMessage($request->input('error'));
        }

        if ($request->has('url')) {
            $vars['url'] = $request->input('url');
        }

        return view('app.index', $vars);
    }

    public function recent($d, $m, $y)
    {
        $date = Carbon::createFromDate($y, $m, $d);
        $vars = compact('date');
        $dateTitle = $date->format('d.m.Y');
        $title = 'Последно свалени MP3 от VBOX7 - ' . $dateTitle;
        $vars['title'] = $title;
        $vars['description'] = $title;
        $recentData = Log::getRecentForDate($date, static::VIDEOS_PER_PAGE);

        return view('app.recent', array_merge($recentData, $vars));
    }

    private static function formatDownloadError(Exception $e, $url, $isXmlHttpRequest = false)
    {
        if ($isXmlHttpRequest) {
            return [
                'code' => $e->getCode(),
                'message' => $e->getMessage(),
            ];
        }

        return redirect(route('index', [
            'url' => $url,
            'error' => $e->getCode(),
        ]));
    }

    public function find(Request $request)
    {
        $input = $request->validate([
            'url' => 'required',
        ]);

        if (preg_match(static::WATCH_YOUTUBE_URL_REGEX, $input['url'], $result)) {
            return static::formatDownloadError(new videoDownloaderException('', -1), $input['url']);
        }

        if (
            !preg_match(static::WATCH_URL_REGEX, $input['url'], $result) ||
            empty($result[1]) ||
            trim($result[1], '01234567890abcdef') !== ''
        ) {
            return redirect(route('search', [
                'q' => $input['url'],
            ]));
        }
		
		$videoId = $result[1];

        try {
			if (in_array($videoId, ['0835001731'], true)) {
				throw new videoDownloaderException('', -2);
			}
            $video = Video::fromVbox7Id($videoId, true);
            return redirect($video->pageUrl);
        }
        catch (Exception $e) {
            return static::formatDownloadError($e, $input['url'], $request->isXmlHttpRequest());
        }
    }

    public function watch($video)
    {
        try {
            /* @var Video $video */
            $video = Video::fromVbox7Id($video);
            $vars = compact('video');
            $vars['title'] = $video->title . ' [' . $video->video_id . ']';
            $vars['description'] = $vars['title'] . ' - mp3 от vbox7, vbox7 downloader, vbox7 mp3';
            $vars['og_title'] = $video->title;
            $vars['og_description'] = config('params.title_suffix');
            return view('app.watch', $vars);
        }
        catch (Throwable $e) {
            $error = $e->getMessage();
            return response()->view('app.error', compact('error'))->setStatusCode(404);
        }
    }

    public function refresh(Request $request)
    {
        $input = $request->validate([
            'id' => 'required',
            'force' => 'boolean|nullable',
        ]);

        /* @var Video $video */
        $video = Video::findOrFail($input['id']);
        $oldVideoId = $video->video_id;
        $exists = $video->updateData($input['force'] ?? false);

        if ($oldVideoId !== $video->video_id) {
            return [
                'redirect' => $video->page_url,
            ];
        }

        if ($exists) {
            $html = view('partials.player', compact('video'))->render();

            return [
                'title' => $video->title,
                'url' => $video->url,
                'subtitleId' => $video->subtitle_id,
                'html' => $html,
                'bestResolution' => $video->best_resolution,
            ];
        }
        else {
            return [
                'error' => 1,
            ];
        }
    }

    public function track(Request $request)
    {
        if (!Crawler::isCrawler()) {
            /* @var Video $video */
            $video = Video::findOrFail($request->query->getInt('video_id'));
            $result = Log::incrementCount($video, $request->query->get('type'));

            if (is_null($result)) {
                return response('', 422);
            }

            if (!$result) {
                return response('', 500);
            }
        }

        $gif = base64_decode('R0lGODlhAQABAIABAP///wAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==');

        return response($gif, 200, [
            'Content-Type' => 'image/gif',
            'Content-Length' => strlen($gif),
        ]);
    }
}
