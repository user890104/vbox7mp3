(function($) {
	var isEdge = navigator.userAgent.indexOf('Edge/') > -1;
	var chromeIdx = navigator.userAgent.indexOf('Chrome/');
	var isChrome = chromeIdx > -1 && !isEdge;
	var isFF = navigator.userAgent.indexOf('Firefox/') > -1;
	var isIe10 = navigator.userAgent.indexOf('MSIE 10') > -1;
	var isAndroidWv = isChrome && navigator.userAgent.indexOf(' Version/') > -1;
	var hasCors = 'XMLHttpRequest' in window && 'withCredentials' in new XMLHttpRequest();
	var hasCou = 'URL' in window && 'createObjectURL' in URL && !isAndroidWv;
	var hasDownload = typeof $('<a />').prop('download') === 'string';
	var hasMsSaveBlob = 'msSaveBlob' in navigator;
	
	if ('console' in window) {
		console.log('isChrome', isChrome);
		console.log('isFF', isFF);
		console.log('isIe10', isIe10);
		console.log('hasMsSaveBlob', hasMsSaveBlob);
		console.log('hasDownload', hasDownload);
		console.log('hasCors', hasCors);
		console.log('hasCou', hasCou);
	}
	
	function clickDownloadLink(opts) {
		var link = $('<a />').attr({
			'href': opts.url,
			'download': opts.filename
		})
		.hide()
		.text(opts.linkText || 'DOWNLOAD')
		.appendTo(opts.linkContainer || document.body);

		var domLink = link.get(0);

		if ('click' in domLink) {
			domLink.click();
			link.remove();

			if (opts.revokeUrl) {
				setTimeout(function() {
					opts.revokeUrl();
				}, 100);
			}
		}

		link.show();
	}

	function canSaveBlob() {
		return hasMsSaveBlob || hasCou;
	}

	function saveBlob(blob, settings) {
		var opts = $.extend(true, {
			'filename': 'file.bin'
		}, settings);
		
		if (!blob) {
			return Promise.reject(new Error('Invalid BLOB object provided'));
		}

		if (hasMsSaveBlob) {
			try {
				navigator.msSaveBlob(blob, isIe10 ? getSlug(opts.filename, {
					'separator': ' ',
					'lang': 'bg',
					'symbols': false,
					'maintainCase': true,
					'uric': true,
					'uricNoSlash': true,
					'mark': true
				}) : opts.filename);
			}
			catch (e) {
				if (e.message === 'Not implemented') {
					return Promise.reject(new Error("msSaveBlob not implemented"));
				}
				
				throw e;
			}
			
			return Promise.resolve();
		}
		
		if (hasCou) {
			opts.url = URL.createObjectURL(blob);
			opts.revokeUrl = function () {
				URL.revokeObjectURL(opts.url);
			};
			
			clickDownloadLink(opts);
			
			return Promise.resolve();
		}
		
		return Promise.reject(new Error('No supported saveBlob method available'));
	}
	
	function xhrFetch(opts) {
		return new Promise(function (resolve, reject) {
			var xhr = new XMLHttpRequest();
			var isRunning = false;
			
			xhr.addEventListener('error', function() {
				reject(new Error('XHR error'));
			}, false);
			
			xhr.addEventListener('readystatechange', function(e) {
				if (xhr.readyState === 3) {
					isRunning = true;
				}
				
				if (xhr.readyState !== 4) {
					return;
				}

				if (xhr.status === 0 && isRunning) {
					reject(new Promise.OperationalError('XHR aborted'));
				}
				
				if (xhr.status !== 200) {
					reject(new Error('XHR failed, status=' + xhr.status));
				}
				
				resolve(xhr.response);
			}, false);

			if ('onProgress' in opts) {
				xhr.addEventListener('progress', opts.onProgress, false);
			}
			
			xhr.open('GET', opts.url);
			xhr.responseType = opts.responseType || 'blob';
			xhr.send();
		});
	}
	
	function downloadCors(opts) {
		return xhrFetch(opts).then(function(response) {
			return saveBlob(response, opts);
		});
	}
	
	function downloadAttribute(opts) {
		clickDownloadLink(opts);
		
		if ('onChromeWarning' in opts) {
			var startPos = chromeIdx;
			var tokenCount = 0;
			
			while ((startPos = navigator.userAgent.indexOf(' ', startPos + 1)) > -1) {
				++tokenCount;
			}
			
			if (tokenCount === 1) {
				opts.onChromeWarning(opts.url.substr(opts.url.lastIndexOf('/') + 1));
			}
		}
		
		return Promise.resolve();
	}
	
	function forceDownload(settings) {
		var opts = $.extend(true, {
			'filename': 'file.bin'
		}, settings);
		
		if (!('url' in opts)) {
			throw new Error('No URL specified');
		}
		
		return Promise.resolve().then(function() {
			if (hasCors && (hasMsSaveBlob || (hasCou && hasDownload))) {
				return downloadCors(opts);
			}
			
			throw new Error('No CORS/saveBlob support');
		})
		.caught(function(e) {
			// XHR cancellation
			if (e instanceof Promise.OperationalError) {
				throw e;
			}
			
			if (hasDownload && !isFF) {
				return downloadAttribute(opts);
			}

			throw new Error('No supported download methods');
		});
	}
	
	$.extend(true, $, {
		canSaveBlob: canSaveBlob,
		saveBlob: saveBlob,
		xhrFetch: xhrFetch,
		forceDownload: forceDownload
	});
})(jQuery);
