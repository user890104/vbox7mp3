<video id="player" class="video-js vjs-big-play-centered" controls poster="{{ $video->picture0 }}" data-setup='{{ json_encode([
    'aspectRatio' => '16:9',
    'nativeControlsForTouch' => true,
    'playbackRates' => [0.25, 0.5, 0.75, 1, 1.25, 1.5, 2],
    'plugins' => [
        'mediaSession' => [
            'title' => $video->title,
            'artist' => 'VBOX7-MP3',
            'artwork' => [[
                'src' => $video->picture0,
                'sizes' => '1280x720',
                'type' => 'image/jpeg'
            ], [
                'src' => $video->picture1,
                'sizes' => '120x68',
                'type' => 'image/jpeg'
            ], [
                'src' => $video->picture2,
                'sizes' => '182x102',
                'type' => 'image/jpeg'
            ]],
        ],
    ],
]) }}'>
    <source src="{{ $video->url }}" type="video/mp4">
    <p>
        <strong>{{ $video->title }}.mp3</strong>
    </p>
</video>
