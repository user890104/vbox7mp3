@extends('layouts.app')

@section('head')
<style id="progress-hack"></style>
@endsection

@section('content')
<div class="content-wrapper content-inner content-watch">
	<h1>{{ $video->title }}</h1>
	@include('partials.fb_like')
	<div class="clearfix"></div>
	<!-- /5599524/VBOX7MP3-Watch-Top -->
	<div id="div-gpt-ad-1536011395894-0" style="width: 728px; height: 90px;" class="banner banner-728">
		<script>
            googletag.cmd.push(function() {
            	googletag.display('div-gpt-ad-1536011395894-0');
			});
		</script>
	</div>
	<div class="video-options-container">
		<div class="video-container">
			@include('partials.player')
		</div>
		<div class="options-container">
			<form action="" method="post" id="download-form" data-config="{{ json_encode([
			    'id' => $video->id,
				'title' => $video->title,
				'videoUrl' => $video->url,
				'subtitleId' => $video->subtitle_id,
				'scriptUrl' => elixir('js/ffmpeg/ffmpeg.js', 'assets'),
				'scriptSize' => filesize(public_path(elixir('js/ffmpeg/ffmpeg.js', 'assets'))),
			]) }}">
				<label class="download audio">
					<input type="submit" name="action" value="audio">
					<span class="hide-320">Свали </span>песен
				</label>
				<label class="download video">
					<input type="submit" name="action" value="video">
                    <span class="hide-320">Свали </span>клип (<span class="bestResolution">{{ $video->best_resolution }}</span>)
				</label>
				@if($video->subtitle_id)
				<label class="download subs">
					<input type="submit" name="action" value="subs">
                    <span class="hide-375">Свали </span>субтитри
				</label>
				@else
					<label class="download subs inactive">
					Няма субтитри
					</label>
				@endif
			</form>
			<button class="problem refresh">🔄</button>
			<a href="{{ route('issues') }}" class="problem">Проблеми<span class="hide-375"> при свалянето</span>?</a>
		</div>
	</div>
	<img src="{{ route('track', [
		'type' => 'watch',
		'video_id' => $video->id,
	]) }}" width="0" height="0">
    <!-- /5599524/VBOX7MP3-Watch-Bottom -->
    <div id="div-gpt-ad-1539959690552-0" style="width: 728px; height: 90px;" class="banner banner-728">
        <script>
            googletag.cmd.push(function() {
                googletag.display('div-gpt-ad-1539959690552-0');
            });
        </script>
    </div>
	@include('partials.bottom')
</div>
@endsection
