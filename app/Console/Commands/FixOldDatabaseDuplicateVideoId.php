<?php

namespace App\Console\Commands;

use DB;
use Illuminate\Console\Command;

class FixOldDatabaseDuplicateVideoId extends Command
{
    protected $signature = 'vbox7mp3:fix_old_dup_vid_id';
    protected $description = 'Fix duplicate video_id in old database';

    public function handle()
    {
        $oldConn = DB::connection('old_mysql');

        $dups = $oldConn->select('
            SELECT
                GROUP_CONCAT(`id`) AS `ids`
            FROM
                `videos`
            GROUP BY
                `video_id`
            HAVING
                COUNT(`id`) > 1
        ');

        $barSteps = count($dups);
        $bar = $this->output->createProgressBar($barSteps);
        $bar->setRedrawFrequency($barSteps / 100);

        foreach ($dups as $dup) {
            $ids = explode(',', $dup->ids);
            $ids = array_map('intval', $ids);
            $main_id = min($ids);

            $dup_ids = array_values(array_filter($ids, function($id) use ($main_id) {
                return $id !== $main_id;
            }));

            $dup_ids_placeholder = implode(', ', array_fill(0, count($dup_ids), '?'));
            $params = $dup_ids;
            array_unshift($params, $main_id);

            $oldConn->beginTransaction();

            $sql = 'UPDATE `logs` SET `video_id` = ? WHERE `video_id` IN (' . $dup_ids_placeholder . ')';
            //echo $sql, PHP_EOL, json_encode($params), PHP_EOL;
            $oldConn->statement($sql, $params);

            $sql = 'DELETE FROM `videos` WHERE `id` IN (' . $dup_ids_placeholder . ')';
            //echo $sql, PHP_EOL, json_encode($dup_ids), PHP_EOL;
            $oldConn->statement($sql, $dup_ids);

            //$oldConn->rollBack();
            $oldConn->commit();
            $bar->advance();
        }

        $bar->finish();
    }
}
