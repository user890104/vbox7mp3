@extends('layouts.app')

@section('content')
<div class="content-wrapper content-inner">
	<h1>
		@yield('text_title')
	</h1>
	@include('partials.fb_like')
	<div class="clearfix"></div>
	<div class="text-content">
	@yield('text_content')
	</div>
	@include('partials.bottom')
</div>
@endsection
