function networkThenCache(event, cacheId) {
    event.respondWith(
        fetch(event.request).then(function(response) {
            const responseForCache = response.clone();

            event.waitUntil(caches.open(cacheId).then(function(cache) {
                return cache.put(event.request, responseForCache);
            }));

            return response;
        }).catch(function() {
            return caches.open(cacheId).then(function(cache) {
                return cache.match(event.request);
            }).then(function (matching) {
                return matching || Promise.reject('no-match');
            });
        })
    );
}

function cacheThenNetwork(event, cacheId) {
    event.respondWith(
        caches.open(cacheId).then(function(cache) {
            return cache.match(event.request).then(function (matching) {
                return matching || Promise.reject('no-match');
            }).catch(function() {
                return fetch(event.request).then(function(response) {
                    const responseForCache = response.clone();

                    event.waitUntil(cache.put(event.request, responseForCache));

                    return response;
                });
            });
        })
    );
}

const cacheConfig = {
    'vbox7mp3-assets': {
        match: function(url) {
            return url.origin === location.origin &&
                url.pathname !== '/track';
        },
        cache: networkThenCache
    },
    'vbox7-imgs': {
        match: function(url) {
            return url.origin === 'https://i49.vbox7.com';
        },
        cache: cacheThenNetwork
    }
};

self.addEventListener('fetch', function(event) {
	// This prevents some weird issue with Chrome DevTools and 'only-if-cached'
	// Fixes issue #385, also ref to:
	// - https://github.com/paulirish/caltrainschedule.io/issues/49
	// - https://bugs.chromium.org/p/chromium/issues/detail?id=823392
	if (event.request.cache === 'only-if-cached' && event.request.mode !== 'same-origin') {
		return;
	}
	
    if (event.request.method !== 'GET') {
        return;
    }

	const url = new URL(event.request.url);

	if (url.protocol !== 'https:') {
		return;
	}
	
    let cacheId = null;

    for (let id in cacheConfig) {
        if (cacheConfig[id].match(url)) {
            cacheId = id;
            break;
    	}
    }

    if (cacheId === null) {
		return;
	}

    cacheConfig[cacheId].cache(event, cacheId);
});
