<?php

namespace App\Http\ViewComposers;

use App\Video;
use Illuminate\View\View;

class MostWatchedVideosComposer
{
    public function compose(View $view)
    {
        $view->with('videos', Video::getMostWatched());
    }
}
