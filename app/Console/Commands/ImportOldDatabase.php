<?php

namespace App\Console\Commands;

use App\Helpers\MassMysqlHelper;
use DB;
use Illuminate\Console\Command;
use Illuminate\Support\Collection;

class ImportOldDatabase extends Command
{
    protected $signature = 'vbox7mp3:import_old_db';
    protected $description = 'Import the old database';

    public function handle()
    {
        $chunkSize = env('CHUNK_SIZE_IMPORT', 1000);

        $newConn = DB::connection();
        $oldConn = DB::connection('old_mysql');

        $count = $oldConn->table('videos')->count();
        $chunks = intdiv($count, $chunkSize) + 1;
        $barSteps = $chunks + 1;

        $bar = $this->output->createProgressBar($barSteps);
        $bar->setRedrawFrequency($barSteps / 100);

        $newConn->table('videos')->truncate();

        $bar->advance();
        $bar->display();

        $oldConn->table('videos')->select(['server_id', 'video_id', 'video_key', 'video_type', 'title'])->orderBy('id')
            ->chunk($chunkSize, function(Collection $rows) use ($newConn, $bar) {
            $newConn->insert(...MassMysqlHelper::generateInsert(
                'videos',
                array_map(function($row) {
                    return [
                        'server_id' => $row->server_id,
                        'key' => $row->video_id,
                        'secret' => $row->video_key,
                        'extension' => [null, 'flv', 'mp4'][$row->video_type],
                        'title' => $row->title,
                    ];
                }, $rows->all())
            ));

            $bar->advance();
        });

        $bar->finish();
    }
}
