@extends('layouts.app.text')

@section('text_title')
Грешка
@endsection

@section('text_content')
<p>
    <strong>{{ $error }}</strong>
</p>
<p>
    <a href="{{ route('index') }}">Сваляне на друг клип</a>
</p>
@endsection
