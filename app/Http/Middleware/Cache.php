<?php
namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Response;

class Cache
{

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @param string $type
     * @return mixed
     * @internal param null|string $guard
     */
    public function handle($request, Closure $next, $type = 'private')
    {
        /* @var Response $response */
		$response = $next($request);

		if (config('app.env') !== 'production') {
			return $response;
		}

        $response->headers->addCacheControlDirective('stale-while-revalidate', 30);
        $response->headers->addCacheControlDirective('stale-if-error', 1200);

        if ($type === 'public') {
            return $response->setPublic();
        }

        return $response->setPrivate();
    }
}
