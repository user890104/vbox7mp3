<?php

use App\Video;
use Illuminate\Database\Seeder;

class VideosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $videoIds = ['3f182f9406','10bbbf8665','6646429276','ffbe486bc4','44e8712ac4','96a8d67f41','619e3611fa','e22b85fa35','71612db71d','69cfafdb72','3b6e308657','3b6e308657','10bbbf8665','10bbbf8665','69cfafdb72','69cfafdb72','06f4952bc8','06f4952bc8','92b9344101','92b9344101','bcd9430de6','bcd9430de6','f41453085d','f41453085d','df35839cce','df35839cce','e2ec2fb579','e2ec2fb579','40866c41d4','40866c41d4','5b64f2a816','5b64f2a816','6b1472b423','6b1472b423','0716cf56c2','0716cf56c2','8e0c8fff5d','8e0c8fff5d','2905a51949','2905a51949','7fbd50a912','7fbd50a912','9255cde6ff','9255cde6ff','5688dd0241','5688dd0241','d6a99707ba','d6a99707ba','c683bc0df3','c683bc0df3','d58c2be98d','d58c2be98d','18f09c22ad','18f09c22ad','02df3150cd','02df3150cd','cb42bb372f','cb42bb372f','bf26cb8fe8','bf26cb8fe8','9deac5f405','9deac5f405','f70b027d91','f70b027d91','b98b765e4a','b98b765e4a','05bd62c228','05bd62c228','68ac1f0e49','68ac1f0e49','c9f292c14c','c9f292c14c','e66fb43019','e66fb43019','15c5f5c527','15c5f5c527','147bda07dd','147bda07dd','c96bea24a8','c96bea24a8','0a1262b744','0a1262b744','a54ea9f507','619e3611fa','c04eaf0c67','ecc546fdcb','12bd96d8a0','4a13afdcea','3f182f9406','00c7900df3','faa89479d8','0716cf56c2','bd0d8382d9','4f2ac16b51','037a91776a','d58c2be98d','384ccb03e0','44daf4d81b','56f8c5cbce','bd37aacdc8','e6c25ee623','6eec60e7ca','2a339fb585','87ad4be7aa','798cebbe46','6b51de982a','36d894bf96','1f98d1bea7','c88a96fe5b','c96bea24a8','05bd62c228','d413e4e821','999ca9bc5b','0d7dbf88d0','960daec072','2ad428d230','96a8d67f41','7fdd9c51ba','bfc68a50f4','25c9ffbe6e','1b726f1bba','3de4d42e7b','9950f9409b','a852302142','a05435e28d','65478d3d48','836e8af224','6e7bc6042a','328d5b3b0b','bf47913f6e','bd30490540','5065f13af4'];

        foreach ($videoIds as $videoId) {
            Video::fromVbox7Id($videoId, true);
        }
    }
}
