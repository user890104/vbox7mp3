<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVideosTable extends Migration
{
    public function __construct()
    {
        DB::getDoctrineSchemaManager()->getDatabasePlatform()->registerDoctrineTypeMapping('enum', 'string');
    }

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('videos', function (Blueprint $table) {
            $table->increments('id');
			$table->tinyInteger('server_id')->unsigned()->nullable();
			// secure_hash
			$table->timestamp('secure_ts')->nullable();
			// video_id
			// video_key
			$table->tinyInteger('resolutions')->unsigned();
			$table->enum('filetype', ['mp4', 'flv']);
			$table->string('title');
			$table->string('uploader');
        });

        DB::statement('ALTER TABLE `videos` ADD `secure_hash` BINARY(16) NULL AFTER `server_id`');

        DB::statement('ALTER TABLE `videos` ADD `video_id` VARBINARY(5) NOT NULL AFTER `secure_ts`');
        DB::statement('ALTER TABLE `videos` ADD `video_key` BINARY(5) NULL AFTER `video_id`');

        Schema::table('videos', function(Blueprint $table) {
            $table->unique('video_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('videos');
    }
}
