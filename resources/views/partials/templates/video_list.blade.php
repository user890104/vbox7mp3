@if (count($videos) > 0)
<div class="video-list">
	<p>{{ $title ?? 'Клипове' }}</p>
	<ul>
		@foreach ($videos as $video)
		<li>
			<a href="{{ $video->pageUrl }}" title="{{ $video->title }}">
				<div class="img-wrapper">
					@include('partials.templates.lazy_image', [
						'url' => $video->picture1,
						'alt' => $video->title,
						'attributes' => [
							'class' => 'content',
						],
					])
					@if ($downloads ?? false)
						<span>{{ $video->countDownloadsToday }} свалян{{ $video->countDownloadsToday === 1 ? 'е' : 'ия' }}</span>
					@endif
					@if ($views ?? false)
						<span>{{ $video->countViewsToday }} гледан{{ $video->countViewsToday === 1 ? 'е' : 'ия' }}</span>
					@endif
				</div>
				<p>{{ $video->title }}</p>
			</a>
		</li>
		@endforeach
	</ul>
</div>
@endif
