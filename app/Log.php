<?php

namespace App;

use Cache;
use DB;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Carbon;
use Jaybizzle\CrawlerDetect\CrawlerDetect;

/**
 * App\Log
 *
 * @property-read \App\Video $video
 * @mixin \Eloquent
 */
class Log extends Model
{
    public $fillable = [
        'video_id',
        'date',
        'count_video',
        'count_audio',
        'count_watch',
    ];

    public $timestamps = false;

    public function video()
    {
        return $this->belongsTo('App\Video');
    }

    public static function getRecentForDate(Carbon $date, $videosPerPage)
    {
        $page = Paginator::resolveCurrentPage();

        return Cache::remember('videos._recent.' . $date->format('Y-m-d') . '.page' . $page, 30,
        function () use ($date, $videosPerPage) {
            $logs = Log::with('video')
                ->where('logs.date', '=', $date->format('Y-m-d'))
                ->where(function($query) {
                    return $query->where('logs.count_video', '>', 0)
                        ->orWhere('logs.count_audio', '>', 0);
                })
                ->paginate($videosPerPage);

            $videos = $logs->map(function($log) {
                return $log->video;
            });

            return compact('logs', 'videos');
        });
    }

    public static function incrementCount(Video $video, $type)
    {
        $CrawlerDetect = new CrawlerDetect;

        if ($CrawlerDetect->isCrawler()) {
            return null;
        }

        if (
            !in_array($type, ['video', 'audio', 'watch'], true) ||
            is_null($video->id)
        ) {
            return null;
        }

        return DB::statement(<<<EOF
            INSERT INTO
              `logs`
            SET
              `video_id` = :video_id,
              `date` = CURDATE(),
              `count_{$type}` = 1
            ON DUPLICATE KEY UPDATE
              `count_{$type}` = `count_{$type}` + 1
EOF
        , [
            'video_id' => $video->id,
        ]);
    }
}
