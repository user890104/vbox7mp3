<?php

use App\Video;
use Illuminate\Database\Seeder;
use Illuminate\Support\Carbon;
use App\Log;

class LogsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [];
        $startDate = Carbon::parse('last month');
        Video::all()->each(function(Video $video) use (&$data, $startDate) {
            $videoId = $video->id;
            $date = clone $startDate;
            for ($i = 0; $i < 90; ++$i) {
                $data[] = [
                    'video_id' => $videoId,
                    'date' => $date->format('Y-m-d'),
                    'count_watch' => rand(0, 100),
                    'count_audio' => rand(0, 20),
                    'count_video' => rand(0, 30),
                ];
                $date->addDay();
            }
        });

        Log::insert($data);
    }
}
