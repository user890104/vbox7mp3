<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Video;

Route::group(['middleware' => 'cache:public'], function() {
	Route::get('/', 'VideoController@index')
	->name('index')
	->middleware(['expire:300']);


	Route::get('последно-свалени-{d}.{m}.{y}{suffix?}', 'VideoController@recent')
	->where('d', '[0-3][0-9]')
	->where('m', '[01][0-9]')
	->where('y', '[0-9]{4}')
	->name('recent')
	->middleware(['expire:600']);


	Route::get('гледай-vbox7-{video}{suffix?}', 'VideoController@watch')
	->where('video', '[a-f0-9]{8,10}')
	->where('title', '.+')
	->name('watch')
	->middleware(['expire:3600']);

	Route::post('гледай-vbox7-{video}{suffix?}', function (Video $video) {
		return '';
		return var_export(Request::all(), true);
	})
	->where('video', '[a-f0-9]{8,10}')
	->where('title', '.*');


	Route::group(['middleware' => 'expire:86400'], function() {
        Route::get('търсене.html', function () {
            return view('app.search');
        })
        ->name('search');

        Route::get('проблеми-при-сваляне.html', function () {
            return view('app.issues');
        })
        ->name('issues');

        Route::get('условия-за-ползване.html', function () {
			return view('app.terms');
		})
		->name('terms');

		Route::get('връзка-с-нас.html', function () {
			return view('app.contacts');
		})
		->name('contacts');
	});

    Route::get('manifest.webmanifest', 'ManifestController@generate');
});

Route::post('find', 'VideoController@find')
    ->where('url', '.+')
    ->name('find');

Route::post('refresh', 'VideoController@refresh')
    ->where('id', '[0-9]+')
    ->name('refresh');

Route::match(['get', 'post'], 'track', 'VideoController@track')
    ->where('video_id', '[0-9]+')
    ->where('type', 'audio|video|watch')
    ->name('track');

Route::get('download/', function() {
        return redirect(route('index'));
    })
    ->name('download');

Route::post('callbacks/github', 'GithubController@webhook');
