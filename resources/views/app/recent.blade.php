@extends('layouts.app')

@section('content')
<div class="content-wrapper content-inner content-recent">
	<h1>Последно свалени клипове и MP3 - {{ $date->format('d.m.Y') }}</h1>
	@include('partials.fb_like')
	<div class="clearfix"></div>
	<!-- /5599524/VBOX7MP3-Recent-Top -->
	<div id="div-gpt-ad-1536011354599-0" style="width: 728px; height: 90px;" class="banner banner-728">
		<script>
            googletag.cmd.push(function() {
            	googletag.display('div-gpt-ad-1536011354599-0');
			});
		</script>
	</div>
	<div class="video-items-wrapper">
		@foreach ($videos as $video)
		<div class="video-item">
			<a href="{{ $video->pageUrl }}" title="{{ $video->title }}">
				<img class="thumb" src="{{ $video->picture2 }}" alt="">
			</a>
			<a href="{{ $video->pageUrl }}" class="title">{{ $video->title }}</a>
		</div>
		@endforeach
	</div>
	{{ $logs->links() }}
	@include('partials.bottom')
</div>
@endsection
