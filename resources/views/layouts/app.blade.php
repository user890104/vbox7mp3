@php
$title = isset($title) ? ($title . ' - ') : '';
$title .= config('params.title_suffix');
@endphp
<!DOCTYPE html>
<html lang="bg">
	<head>
		<title>{{ $title ?? config('app.name') }}</title>
		<meta charset="utf-8">
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<meta http-equiv="Content-Language" content="bg">
		<meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1">
		<meta name="language" content="bulgarian">
		<meta name="title" content="{{ $title ?? config('app.name') }}">
		<meta name="description" content="{{ $description ?? config('params.description') }}">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="apple-mobile-web-app-capable" content="yes">
		<meta name="apple-mobile-web-app-title" content="{{ config('app.name') }}">
		<meta name="apple-mobile-web-app-status-bar-style" content="{{ config('params.theme_color') }}">
		<meta name="theme-color" content="{{ config('params.theme_color') }}">
		<meta name="msapplication-navbutton-color" content="{{ config('params.theme_color') }}">
		<meta property="og:site_name" content="{{ config('app.name') }}">
		<meta property="fb:app_id" content="{{ env('FACEBOOK_APP_ID') }}">
		<meta property="og:title" content="{{ $og_title ?? ($title ?? config('app.name')) }}">
		<meta property="og:description" content="{{ $og_description ?? ($description ?? config('params.description')) }}">
		<meta property="og:url" content="{{ URL::current() }}">
		@if(isset($video))
			<meta name="medium" content="video">
			<meta property="og:type" content="article">
			<meta property="og:image" content="{{ $video->picture0 }}">
			<meta property="og:image:width" content="{{ $video->image_width ?? 1280 }}">
			<meta property="og:image:height" content="{{ $video->image_height ?? 720 }}">
			<link rel="image_src" href="{{ $video->picture0 }}">
		@else
			<meta property="og:type" content="website">
			<meta property="og:image" content="{{ elixir('img/logo.png', 'assets') }}">
			<meta property="og:image:width" content="270">
			<meta property="og:image:height" content="60">
		@endif
		<link rel="manifest" href="/manifest.webmanifest">
		<link rel="canonical" href="{{ URL::current() }}">
		<link rel="apple-touch-icon-precomposed" sizes="152x152" href="{{ elixir('img/icons/152.png', 'assets') }}">
		<link rel="apple-touch-icon-precomposed" sizes="144x144" href="{{ elixir('img/icons/144.png', 'assets') }}">
		<link rel="apple-touch-icon-precomposed" sizes="76x76" href="{{ elixir('img/icons/76.png', 'assets') }}">
		<link rel="apple-touch-icon-precomposed" sizes="72x72" href="{{ elixir('img/icons/72.png', 'assets') }}">
		<link rel="apple-touch-icon-precomposed" sizes="120x120" href="{{ elixir('img/icons/180.png', 'assets') }}">
		<link rel="apple-touch-icon-precomposed" sizes="57x57" href="{{ elixir('img/icons/57.png', 'assets') }}">
		<link rel="apple-touch-startup-image" media="(device-width: 768px) and (device-height: 1024px) and (-webkit-device-pixel-ratio: 2) and (orientation: portrait)" href="{{ elixir('img/splash/1536x2008.png', 'assets') }}">
		<link rel="apple-touch-startup-image" media="(device-width: 768px) and (device-height: 1024px) and (-webkit-device-pixel-ratio: 2) and (orientation: landscape)" href="{{ elixir('img/splash/1496x2048.png', 'assets') }}">
		<link rel="apple-touch-startup-image" media="(device-width: 768px) and (device-height: 1024px) and (-webkit-device-pixel-ratio: 1) and (orientation: portrait)" href="{{ elixir('img/splash/768x1004.png', 'assets') }}">
		<link rel="apple-touch-startup-image" media="(device-width: 768px) and (device-height: 1024px) and (-webkit-device-pixel-ratio: 1) and (orientation: landscape)" href="{{ elixir('img/splash/748x1024.png', 'assets') }}">
		<link rel="apple-touch-startup-image" media="(device-width: 414px) and (device-height: 736px) and (-webkit-device-pixel-ratio: 3) and (orientation: portrait)" href="{{ elixir('img/splash/1242x2148.png', 'assets') }}">
		<link rel="apple-touch-startup-image" media="(device-width: 414px) and (device-height: 736px) and (-webkit-device-pixel-ratio: 3) and (orientation: landscape)" href="{{ elixir('img/splash/1182x2208.png', 'assets') }}">
		<link rel="apple-touch-startup-image" media="(device-width: 375px) and (device-height: 667px) and (-webkit-device-pixel-ratio: 2)" href="{{ elixir('img/splash/750x1294.png', 'assets') }}">
		<link rel="apple-touch-startup-image" media="(device-width: 320px) and (device-height: 568px) and (-webkit-device-pixel-ratio: 2)" href="{{ elixir('img/splash/640x1096.png', 'assets') }}">
		<link rel="apple-touch-startup-image" media="(device-width: 320px) and (device-height: 480px) and (-webkit-device-pixel-ratio: 2)" href="{{ elixir('img/splash/640x920.png', 'assets') }}">
		<link rel="apple-touch-startup-image" media="(device-width: 320px) and (device-height: 480px) and (-webkit-device-pixel-ratio: 1)" href="{{ elixir('img/splash/320x460.png', 'assets') }}">
		<link rel="stylesheet" href="{{ elixir('css/app.css', 'assets') }}">
		<script src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js" async="async" data-ad-client="ca-pub-0093241427284270"></script>
		<script src="https://www.googletagmanager.com/gtag/js?id={{ env('GOOGLE_ANALYTICS_ID') }}" async="async"></script>
		<script src="https://www.googletagservices.com/tag/js/gpt.js" async="async"></script>
		<script>
			// Google Analytics
            window.dataLayer = window.dataLayer || [];
            function gtag(){dataLayer.push(arguments);}
            gtag('js', new Date());
            gtag('config', '{{ env('GOOGLE_ANALYTICS_ID') }}');

			// Google DFP
            var googletag = googletag || {};
            googletag.cmd = googletag.cmd || [];
            googletag.cmd.push(function() {
                googletag.defineSlot('/5599524/VBOX7MP3-Header', [468, 60], 'div-gpt-ad-1536010613134-0').addService(googletag.pubads());
                googletag.defineSlot('/5599524/VBOX7MP3-Index-Right', [336, 280], 'div-gpt-ad-1536011290581-0').addService(googletag.pubads());
                googletag.defineSlot('/5599524/VBOX7MP3-Index-Bottom', [728, 90], 'div-gpt-ad-1536011241347-0').addService(googletag.pubads());
                googletag.defineSlot('/5599524/VBOX7MP3-Recent-Top', [728, 90], 'div-gpt-ad-1536011354599-0').addService(googletag.pubads());
                googletag.defineSlot('/5599524/VBOX7MP3-Watch-Top', [728, 90], 'div-gpt-ad-1536011395894-0').addService(googletag.pubads());
                googletag.defineSlot('/5599524/VBOX7MP3-Watch-Bottom', [728, 90], 'div-gpt-ad-1539959690552-0').addService(googletag.pubads());
                googletag.defineSlot('/5599524/VBOX7MP3-Footer', [728, 90], 'div-gpt-ad-1536011157216-0').addService(googletag.pubads());
                googletag.pubads().enableSingleRequest();
                googletag.enableServices();
            });
		</script>
		@yield('head')
	</head>
	<body>
		<div class="global-wrapper">
			<header>
				<div class="content-inner">
					<a href="{{ route('index') }}" class="logo">
						<img src="{{ elixir('img/logo.png', 'assets') }}" width="270" height="60"
							alt="{{ config('app.name') }}">
					</a>
					<!-- /5599524/VBOX7MP3-Header -->
					<div id="div-gpt-ad-1536010613134-0" style="width: 468px; height: 60px;" class="banner banner-468">
						<script>
							googletag.cmd.push(function() {
								googletag.display('div-gpt-ad-1536010613134-0');
							});
						</script>
					</div>
				</div>
			</header>
			<div id="content">
                @yield('content')
			</div>
		</div>
		<footer>
			<div class="content-inner">
				<div>
					<span>Последно свалени клипове и MP3 от VBOX7 по дати:</span>
					@for ($i = 0; $i < 10; ++$i)
						@php
							$ts = strtotime('-' . $i . ' days');
						@endphp
						<a href="{{ route('recent', [
							'd' => date('d', $ts),
							'm' => date('m', $ts),
							'y' => date('Y', $ts),
							'suffix' => '.html',
						]) }}">{{ date('d.m.Y', $ts) }}</a>
					@endfor
				</div>
				<br>
				<ul>
					<li><a href="{{ route('issues') }}">Проблеми със свалянето?</a></li>
					<li><a href="{{ route('terms') }}">Условия за ползване</a></li>
					<li><a href="{{ route('contacts') }}">Връзка с нас</a></li>
				</ul>
				<p>© 2010-{{ date('Y') }} Венцислав "Slackware" Атанасов</p>
			</div>
		</footer>
        <script type="application/ld+json">
			{
				"@context": "http://schema.org",
				"@type": "WebSite",
				"url": "{{ route('index') }}",
				"potentialAction": {
					"@type": "SearchAction",
					"target": "{{ route('search') }}?q={search_term_string}",
					"query-input": "required name=search_term_string"
				}
			}
        </script>
        <script type="text/javascript" src="{{ elixir('js/app.js', 'assets') }}" defer></script>
        @yield('footer')
		<div id="fb-root"></div>
		<script>
            window.fbAsyncInit = function() {
                FB.init({
                    appId            : '{{ env('FACEBOOK_APP_ID') }}',
                    autoLogAppEvents : true,
                    xfbml            : true,
                    version          : 'v3.0'
                });
            };

            (function(d, s, id){
                var js, fjs = d.getElementsByTagName(s)[0];
                if (d.getElementById(id)) {return;}
                js = d.createElement(s); js.id = id;
                js.src = 'https://connect.facebook.net/bg_BG/sdk.js';
                fjs.parentNode.insertBefore(js, fjs);
            }(document, 'script', 'facebook-jssdk'));
		</script>
	</body>
</html>
<!-- ---------------------------------
 _
( \
 ) )                  ________________
( (  .-""-.  ^.-.^   |I CAN HAZ HTML?|
 \ \/      \/ , , \  /_______________|
  \   \    =;  t  /=
   \   |"".  ',--'
    / //  | ||
   /_,))  |_,))
################################## -->
