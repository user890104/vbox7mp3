<?php

namespace App\Helpers;

use Exception;

class MassMysqlHelper {
    public static function generateInsert($table, array $data)
    {
        if (count($data) === 0) {
            return false;
        }

        $columns = array_keys(reset($data));

        if (count($columns) === 0) {
            return false;
        }

        $sql_cols = implode('`, `', $columns);
        $sql_params = implode('), (', array_fill(0, count($data), implode(', ', array_fill(0, count($columns), '?'))));

        $sql = <<<EOF
INSERT INTO
    `${table}` (`${sql_cols}`)
VALUES
    (${sql_params})
EOF;

        $params = [];

        foreach ($data as $idx => $row) {
            foreach ($columns as $col) {
                if (!array_key_exists($col, $row)) {
                    throw new Exception('Missing column ' . $col . ' in row ' . $idx);
                }

                $params[] = $row[$col];
            }
        }

        return [$sql, $params];
    }

    public static function generateUpdate($table, array $data, $key_name = 'id')
    {
        if (count($data) === 0) {
            return false;
        }

        $columns = array_keys(reset($data));

        if (count($columns) === 0) {
            return false;
        }

        $fields = [];

        foreach ($columns as $field) {
            $fields[$field] = [];
        }

        foreach ($data as $key => $values) {
            foreach ($values as $field => $value) {
                if (!array_key_exists($field, $fields)) {
                    throw new Exception('Missing column ' . $field . ' in row ' . $key);
                }

                $fields[$field][$key] = $value;
            }
        }

        $sql_fields = [];
        $params = [];

        foreach ($fields as $field => $values) {
            $sql_field = <<<EOF
    `${field}` = CASE

EOF;

            foreach ($values as $key => $value) {
                $sql_field .= <<<EOF
        WHEN `id` = ? THEN ?

EOF;
                array_push($params, $key, $value);
            }

            $sql_field .= <<<EOF
    END
EOF;
            $sql_fields[] = $sql_field;
        }

        $sql_fields = implode(',' . "\n", $sql_fields);
        $sql_keys = implode(', ', array_fill(0, count($data), '?'));

        array_push($params, ...array_keys($data));

        $sql = <<<EOF
UPDATE
    `${table}`
SET
${sql_fields}
WHERE
    `${key_name}` IN (${sql_keys})
EOF;

        return [$sql, $params];
    }
}
