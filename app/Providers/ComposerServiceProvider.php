<?php

namespace App\Providers;

use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

class ComposerServiceProvider extends ServiceProvider
{
    /**
     * Register bindings in the container.
     *
     * @return void
     */
    public function boot()
    {
        View::composer('partials.chosen_videos', 'App\Http\ViewComposers\ChosenVideosComposer');
        View::composer('partials.most_downloaded_videos', 'App\Http\ViewComposers\MostDownloadedVideosComposer');
        View::composer('partials.most_watched_videos', 'App\Http\ViewComposers\MostWatchedVideosComposer');
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
