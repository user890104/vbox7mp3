<?php

return [
    'description' => 'Сваляне на клипове и MP3 от VBOX7',
    'title_suffix' => 'MP3 от vbox7.com - vbox to mp3 downloader, bezplatno mp3, безплатно теглене на клипове',
    'lang' => 'bg-BG',
    'name' => 'Сваляне от VBOX7',
    'theme_color' => '#9cc4eb',
];
