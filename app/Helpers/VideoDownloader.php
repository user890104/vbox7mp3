<?php
/**
 * Created by PhpStorm.
 * User: Venci
 * Date: 31.3.2018 г.
 * Time: 17:28 ч.
 */

namespace App\Helpers;

use App\Helpers\Exception\videoDownloaderException\temporaryException\apiException;
use App\Helpers\Exception\videoDownloaderException\temporaryException\curlException;
use App\Helpers\Exception\videoDownloaderException\temporaryException\downloadException;
use App\Video;

class VideoDownloader
{
    private static $metadataUrl = 'https://www.vbox7.com/ajax/video/nextvideo.php?vid=%s';

    private static $mediaUrlRegex = '#^(?:https?:)?//media(?P<server_id>\d+)?\.vbox7\.com/s/[a-f0-9]{2}/(?P<video_id>[a-f0-9]{8,10})(?:r(?P<video_key>[a-f0-9]{9}))?\.(?P<filetype>.+)$#';
    private static $mediaUrlRegex2 = '#^(?:https?:)?//media(?P<server_id>\d+)?\.vbox7\.com/s/[a-f0-9]{2}/(?P<video_id>[a-f0-9]{8,10})/[a-f0-9]{8,10}(?:r(?P<video_key>[a-f0-9]{9}))(?:_(?P<resolution>.*))?\.(?P<filetype>.+)$#';
    private static $mediaUrlRegex3 = '#^(?:https?:)?//(?P<server_type>media|edge)(?P<server_id>\d+)?\.vbox7\.com/sl/(?P<secure_hash>[a-zA-Z0-9_-]{22})/(?P<secure_ts>[0-9]{10})/[a-f0-9]{2}/(?:[a-f0-9]{8,10}/)?(?P<video_id>[a-f0-9]{8,10})(?:r(?P<video_key>[a-f0-9]{9}))?(?:_(?P<resolution>.*))?\.(?P<filetype>.+)$#';

    private static $redirectRegex = '#^<script>window\.location\.href = \'/play:(?P<video_id>[a-f0-9]{8,10})\';</script>$#';

    private static function getUserAgent() {
        return 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537.36';
    }

    private static $remainingRedirects = 1;

    private static function getVideoMetadata($videoId) {
        $ch = curl_init();

        if ($ch === false) {
            throw new curlException('cURL init failed');
        }

        if (curl_setopt_array($ch, array(
                CURLOPT_URL => sprintf(static::$metadataUrl, $videoId),
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_AUTOREFERER => true,
                CURLOPT_CONNECTTIMEOUT => 5,
                CURLOPT_TIMEOUT => 10,
                CURLOPT_MAXREDIRS => 5,
                CURLOPT_USERAGENT => static::getUserAgent(true),
                CURLOPT_REFERER => 'https://www.vbox7.com/play:' . $videoId,
                CURLOPT_PROXY => env('PROXY_METADATA'),
            )) === false) {
            $error = curl_error($ch);
            $errno = curl_errno($ch);
            curl_close($ch);
            throw new curlException($error, $errno);
        }

        $response = curl_exec($ch);

        if ($response === false) {
            $error = curl_error($ch);
            $errno = curl_errno($ch);
            curl_close($ch);
            throw new curlException($error, $errno);
        }

        curl_close($ch);

        if (preg_match(static::$redirectRegex, $response, $matches)) {
            if (static::$remainingRedirects > 0) {
                static::$remainingRedirects--;
                return static::getVideoMetadata($matches['video_id']);
            }
        }

        $metadata = json_decode($response, true);

        if (is_null($metadata)) {
            throw new downloadException('Грешка при извличането на информация за клипа, моля опитайте по-късно или се свържете с нас ако проблемът продължава.');
        }

        if (!array_key_exists('success', $metadata) || $metadata['success'] !== true) {
            throw new downloadException('Нямаме достъп до този клип, моля опитайте след 10-15 минути или се свържете с нас ако проблемът продължава.');
        }

        if (!array_key_exists('options', $metadata)) {
            throw new downloadException('Грешка в отговора от сървъра, моля свържете се с нас.');
        }

        $data = $metadata['options'];

        if (array_key_exists('sysMsgType', $data)) {
            throw new downloadException(static::getErrorMessage($data['sysMsgType']), $data['sysMsgType']);
        }

        if (
            !array_key_exists('title', $data) ||
            !array_key_exists('src', $data) ||
            !array_key_exists('uploader', $data)
        ) {
            throw new downloadException('Получихме непълна информация за този клип, и не е възможно свалянето.');
        }

        $data['title'] = html_entity_decode($data['title'], ENT_QUOTES, 'utf-8');

        return $data;
    }

    private static function getHeaders($url) {
        $ch = curl_init();

        if ($ch === false) {
            throw new curlException('cURL init failed');
        }

        if (curl_setopt_array($ch, array(
                CURLOPT_URL => $url,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_AUTOREFERER => true,
                CURLOPT_CONNECTTIMEOUT => 5,
                CURLOPT_TIMEOUT => 10,
                CURLOPT_MAXREDIRS => 5,
                CURLOPT_USERAGENT => static::getUserAgent(),
                CURLOPT_PROXY => env('PROXY_CHECK'),
                CURLOPT_HEADER => true,
                CURLOPT_NOBODY => true,
            )) === false) {
            $error = curl_error($ch);
            $errno = curl_errno($ch);
            curl_close($ch);
            throw new curlException($error, $errno);
        }

        $headers = curl_exec($ch);

        if ($headers === false) {
            $error = curl_error($ch);
            $errno = curl_errno($ch);
            curl_close($ch);
            throw new curlException($error, $errno);
        }

        $url = curl_getinfo($ch, CURLINFO_EFFECTIVE_URL);

        curl_close($ch);

        $filename = pathinfo($url, PATHINFO_FILENAME);

        if (in_array($filename, ['na', 'natm'])) {
            return [];
        }

        $lineToken = "\r\n";
        $endToken = str_repeat($lineToken, 2);

        if (strrpos($headers, $endToken) !== strlen($headers) - strlen($endToken)) {
            return [];
        }

        // Look for additional proxy headers
        $headerStartPos = strrpos($headers, $endToken, -(strlen($endToken) + 1));

        // Cut them if they exist
        if ($headerStartPos !== false) {
            $headers = substr($headers, $headerStartPos + strlen($endToken));
        }

        $headers = explode("\r\n", rtrim($headers));

        $result = array(array_shift($headers)); // status code is always the first

        foreach ($headers as $header) {
            $pos = strpos($header, ':');

            $result[substr($header, 0, $pos)] = ltrim(substr($header, $pos + 1));
        }

        return $result;
    }

    public static function validateUrl($url) {
        try {
            $headers = static::getHeaders($url);
        }
        catch (Exception $e) {
            return false;
        }

        return !empty($headers) && $headers[0] === 'HTTP/1.1 200 OK';
    }

    private static function validMatch(array $matches, $key) {
        if (!array_key_exists($key, $matches)) {
            return false;
        }

        if (strlen($matches[$key]) === 0) {
            return false;
        }

        return $matches[$key];
    }

    private static function parseUrl($url) {
        $result = [
			'server_id' => null,
			'secure_hash' => null,
			'secure_ts' => null,
			'video_key' => null,
			'filetype' => null,
		];

        $queryPos = strpos($url, '?');

        if ($queryPos !== false) {
            $url = substr($url, 0, $queryPos);
        }

        if (preg_match(static::$mediaUrlRegex3, $url, $matches)) {
            if (static::validMatch($matches, 'secure_ts') && static::validMatch($matches, 'secure_hash')) {
                $result['secure_ts'] = intval($matches['secure_ts']);
                $result['secure_hash'] = $matches['secure_hash'];
            }

            if (static::validMatch($matches, 'server_id') && static::validMatch($matches, 'server_type')) {
                $server_id = intval($matches['server_id']);

                if ($matches['server_type'] === 'media') {
                    $result['server_id'] = $server_id;
                }
                elseif ($matches['server_type'] === 'edge') {
                    $result['server_id'] = $server_id % 100;
                }
                else {
                    throw new downloadException('Unknown server type');
                }
            }

            if (static::validMatch($matches, 'video_key')) {
                $result['video_key'] = $matches['video_key'];
            }

            if (static::validMatch($matches, 'filetype')) {
                $result['filetype'] = $matches['filetype'];
            }

			return $result;
        }

        if (preg_match(static::$mediaUrlRegex2, $url, $matches)) {
            if (static::validMatch($matches, 'server_id')) {
                $result['server_id'] = intval($matches['server_id']);
            }

            if (static::validMatch($matches, 'video_key')) {
                $result['video_key'] = $matches['video_key'];
            }

            if (static::validMatch($matches, 'filetype')) {
                $result['filetype'] = $matches['filetype'];
            }

			return $result;
        }

        if (preg_match(static::$mediaUrlRegex, $url, $matches)) {
            if (static::validMatch($matches, 'server_id')) {
                $result['server_id'] = intval($matches['server_id']);
            }

            if (static::validMatch($matches, 'video_key')) {
                $result['video_key'] = $matches['video_key'];
            }

            if (static::validMatch($matches, 'filetype')) {
                $result['filetype'] = $matches['filetype'];
            }

			return $result;
        }

		throw new downloadException('Unable to parse URL: ' . $url);
    }

    private static function mapResolutions(array $resolutions)
    {
        $result = [];

        foreach ($resolutions as $resolution) {
            $index = array_search($resolution, Video::RESOLUTIONS, true);

            if ($index === 0 || $index === false) {
                continue;
            }

            $result[] = $index;
        }

        return $result;
    }

    private static function parseResolutions(array $metadata)
    {
        if (count($metadata['resolutions']) > 0) {
            return static::mapResolutions($metadata['resolutions']);
        }

        $resolutions = [];

        if ($metadata['videoIsHD']) {
            $resolutions[] = 4;

            if ($metadata['videoHasLD']) {
                $resolutions[] = 3;
            }
        }
        else {
            $resolutions[] = 3;
        }

        return $resolutions;
    }

    private static function packResolutions(array $resolutions)
    {
        $result = 0;

        foreach ($resolutions as $res) {
            $result |= (1 << $res);
        }

        return $result;
    }

    private static function buildData(array $metadata)
    {
        if (!static::validateUrl($metadata['src'])) {
            throw new apiException('В момента не е възможно свалянето на този клип поради технически проблем. Моля опитайте по-късно.');
        }

        $urlData = static::parseUrl($metadata['src']);
        $resolutions = static::parseResolutions($metadata);

        if (in_array($urlData['filetype'], ['m3u8', 'mpd'])) {
            $urlData['filetype'] = 'mp4';
        }

        if (!in_array($urlData['filetype'], ['flv', 'mp4'])) {
            throw new downloadException('Unknown file type: ' . $urlData['filetype']);
        }

        $duration = $metadata['duration'] ?? null;
        $subtitleId = $metadata['subtitleId'] ?: null;

        $imageData = [];

        $imageUrl = Video::generatePictureUrl($metadata['vid']);
        $imageFile = file_get_contents($imageUrl);

        if ($imageFile !== false) {
            $imageSize = ImageHelper::getJPEGImageXY($imageFile);

            if ($imageSize !== false && $imageSize['X'] > 0 && $imageSize['Y'] > 0) {
                $imageData = [
                    'image_width' => $imageSize['X'],
                    'image_height' => $imageSize['Y'],
                ];
            }
        }

        $dbData = array_merge([
            'video_id' => $metadata['vid'],
            'resolutions' => static::packResolutions($resolutions),
            'duration' => $duration,
            'subtitle_id' => $subtitleId,
            'title' => $metadata['title'],
            'uploader' => $metadata['uploader'],
        ], $urlData, $imageData);

        return $dbData;
    }

    public static function getData($videoId)
    {
        $metadata = static::getVideoMetadata($videoId);
        $dbData = static::buildData($metadata);
        return $dbData;
    }

    public static function getErrorMessage($code)
    {
        $errorMessages = [
            -2 => 'Това видео не е достъпно заради неуредени авторски права.',
            -1 => 'Избрали сте клип от YouTube, който се показва през сайта на VBOX7, но не се съхранява там. За момента не е възможно тегленето на този вид клипове. Моля да ни извините!',
            100 => 'Този клип не съществува :(',
            101 => 'Личен клип! Собственикът на този клип го е отбелязал като личен. В момента само той/тя могат да гледат този клип.',
            102 => 'Клип за приятели! Собственикът на този клип е отбелязал, че може да се гледа само от потребители, които е добавил/а в приятели.',
            103 => 'Достъпът до това съдържание е спрян по искане на собствениците на авторските му права. Собствениците на авторските права върху този клип са подали искане да бъде премахнат от сайта.',
            104 => 'Личен клип! Собственикът на този клип го е отбелязал като скрит. В момента само той/тя могат да гледат този клип.',
            // 105 => 'Видеото е качено успешно и ще се появи съвсем скоро! :)',
            105 => 'Видеоклипът е качен съвсем скоро, и още не е конвертиран. Моля опитайте отново след няколко минути.',
            // 106 => 'Видеото е качено успешно и се конвертира! :)',
            106 => 'Видеоклипът е качен съвсем скоро, и още не е конвертиран. Моля опитайте отново след няколко минути.',
            // 107 => 'Видеото е качено успешно и се конвертира! :)',
            107 => 'Видеоклипът е качен съвсем скоро, и още не е конвертиран. Моля опитайте отново след няколко минути.',
            // 108 => 'Потребителят все още няма качени видеа.',
            // 109 => 'Все още нямаш качени видеа. Може да го направиш от <a href="https://www.vbox7.com/upload/">тук</a>.',
            // 110 => 'Съдържание за възрастни! За да видиш това клипче, трябва да си логнат!',
            110 => 'Съдържание за възрастни! Не е възможно свалянето на този клип от сайта ни.',
        ];

        $defaultMessage = 'Не е възможно да намерим информация за този клип!';

        if (array_key_exists($code, $errorMessages)) {
            return $errorMessages[$code];
        }

        return $defaultMessage;
    }
}
