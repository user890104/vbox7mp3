<?php

namespace App\Http\Controllers;

use App;
use File;

class ManifestController extends Controller
{
    public function generate()
    {
        $template = [
            'background_color' => config('params.theme_color'),
            'description' => config('params.description'),
            'dir' => 'ltr',
            'display' => 'standalone',
            'icons' => [],
            'lang' => config('params.lang'),
            'name' => config('params.name'),
            'scope' => '/',
            'short_name' => config('app.name'),
            'start_url' => '/?utm_source=a2hs&utm_medium=android&utm_campaign=webapp',
            'theme_color' => config('params.theme_color'),
        ];

        $iconSizes = [36, 48, 72, 96, 144, 192, 256, 384, 512];

        foreach ($iconSizes as $size) {
            $sizeText = $size . 'x' . $size;

            $template['icons'][] = [
                'src' => elixir('img/icons/' . $size . '.png', 'assets'),
                'sizes' => $sizeText,
                'type' => 'image/png',
            ];
        }

        $json = json_encode($template);

        File::put(public_path('manifest.webmanifest'), $json);

        return $template;
    }
}
