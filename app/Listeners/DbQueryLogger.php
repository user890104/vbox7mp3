<?php

namespace App\Listeners;

use Illuminate\Database\Events\QueryExecuted;
use Log;

class DbQueryLogger
{
    public function handle(QueryExecuted $event)
    {
        if (env('DEBUG_SQL_PRINT')) {
            var_dump(
                'Query executed',
                $event->connectionName,
                $event->sql,
                $event->bindings,
                $event->time
            );
        }

        if (env('DEBUG_SQL_LOG')) {
            Log::info(var_export([
                'Query executed',
                $event->connectionName,
                $event->sql,
                $event->bindings,
                $event->time,
            ], true));
        }
    }
}
