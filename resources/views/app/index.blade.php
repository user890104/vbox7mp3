@extends('layouts.app')

@section('content')
<div class="content-wrapper content-inner">
	<p>Здравейте и добре дошли в нашия нов уебсайт! Тук можете да сваляте клипове и песни от <a href="https://www.vbox7.com/" rel="nofollow noopener" target="_blank">VBOX7.com</a>. Свалянето е много лесно - копирайте линк-а на клипа от VBOX7 и го поставете в полето по-долу. След това натиснете бутона <span class="download-btn">Свали</span>. При проблеми със свалянето <a href="{{ route('issues') }}">вижте тук</a>.</p>
	@include('partials.fb_like')
	<div class="clearfix"></div>
	<div class="row">
		<form method="post" action="{{ route('find') }}" id="download">
			<h1>
				<label for="query">Въведете линк от VBOX7.com</label>
			</h1>
			<input type="text" name="url" id="query" required="required" value="{{ $url ?? '' }}">
			<div id="download-error">{{ $error ?? '' }}</div>
			<div id="checkbox-terms">
				<label>
					<input type="checkbox" required="required">
					Съгласявам се с <a href="{{ route('terms') }}">Условията за ползване</a>
				</label>
			</div>
			<button type="submit"> Свали</button>
			<a href="{{ route('issues') }}" class="problem">Проблеми при сваляне?</a>
		</form>
		<!-- /5599524/VBOX7MP3-Index-Right -->
		<div id="div-gpt-ad-1536011290581-0" style="width: 336px; height: 280px;" class="banner banner-336">
			<script>
                googletag.cmd.push(function() {
                	googletag.display('div-gpt-ad-1536011290581-0');
				});
			</script>
		</div>
	</div>
	<!-- /5599524/VBOX7MP3-Index-Bottom -->
	<div id="div-gpt-ad-1536011241347-0" style="width: 728px; height: 90px;" class="banner banner-728">
		<script>
            googletag.cmd.push(function() {
            	googletag.display('div-gpt-ad-1536011241347-0');
			});
		</script>
	</div>
	@include('partials.bottom')
</div>
@endsection
