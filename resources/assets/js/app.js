// break out of frames
if (self !== top) {
    top.location.replace(location.origin || location);
}

jQuery(function($) {
    // Service worker
    if ('serviceWorker' in navigator) {
        navigator.serviceWorker.register('/sw.js');
    }

    // Facebook like box
    var xfbml_timer = null;

    $(window).on('resize', function() {
        if (xfbml_timer !== null) {
            clearTimeout(xfbml_timer);
            xfbml_timer = null;
        }

        xfbml_timer = setTimeout(function() {
            xfbml_timer = null;

            //fb_iframe_widget class is added after first FB.FXBML.parse()
            //fb_iframe_widget_fluid is added in same situation, but only for mobile devices (tablets, phones)
            //By removing those classes FB.XFBML.parse() will reset the plugin widths.
            var xfbml_element = $('.fb-box.right > .fb-page');
            xfbml_element.removeClass('fb_iframe_widget fb_iframe_widget_fluid');
            if (window.FB) {
                FB.XFBML.parse(xfbml_element.parent().get(0));
            }
        }, 1000);
    });

    // Lazy loading
    function isInViewport(el) {
        var rect = el.getBoundingClientRect();

        return (
            rect.bottom >= 0 &&
            rect.right >= 0 &&

            rect.top <= (
                innerHeight ||
                document.documentElement.clientHeight) &&

            rect.left <= (
                innerWidth ||
                document.documentElement.clientWidth)
        );
    }

    function loadImages() {
        $('noscript.lazy-image').each(function() {
            var $this = $(this);

            if (!isInViewport($this.parent().get(0))) {
                return;
            }

            $this.replaceWith($this.text());
        });
    }

    $(window).on('scroll resize', loadImages);
    loadImages();

    // Download
    var download_form = $('#download-form');
    var labels = download_form.children('label.download');
    var buttons = labels.children('input:submit');

    buttons.on('click', function() {
        var $this = $(this);
        labels.not($this.parent()).addClass('disabled');
        download_form.data('action', $this.val());
    });

    download_form.on('submit', function(e) {
        e.preventDefault();
        var $this = $(this);
        var opts = $this.data('config');
        opts.type = $this.data('action');
        var downloadConfig = download_form.data('config');
        opts.id = downloadConfig.id;
        opts.subtitleId = downloadConfig.subtitleId;
        buttons.prop('disabled', true);

        var videoJsApi = videojs('player');
        var playerDom = videoJsApi.tech(true).el();

        if (playerDom.networkState === playerDom.NETWORK_NO_SOURCE) {
            console.log('recover videoDom noSource');
            recoverPlayback(videoJsApi);
            return;
        }

        $.vbox7Download(opts).caught(function() {
            console.log('recover vbox7Download caught');
            recoverPlayback();
        }).lastly(function() {
            labels.removeClass('disabled');
            buttons.prop('disabled', false);
        });
    });

    // Recover
    var triedToRecover = false;

    function recoverPlayback(api, force) {
        force = force || false;

        if (triedToRecover) {
            return;
        }

        triedToRecover = true;

        // Show message
        if (api) {
            api.errorDisplay.close();
            var message = api.createModal('Търсене на видео клипа. Моля, изчакайте...', {
                unclosable: true
            });
        }

        // Disable download buttons
        buttons.prop('disabled', true);
        labels.addClass('disabled');

        // Request correct URL
        var downloadConfig = download_form.data('config');

        $.ajax({
            type: 'POST',
            url: '/refresh',
            data: {
                id: downloadConfig.id,
                force: force ? 1 : 0
            }
        }).done(function(data, textStatus, jqXHR) {
            if ('redirect' in data) {
                location.replace(data.redirect);
                return;
            }

            if (!('url' in data) || !('title' in data) || !('html' in data)) {
                location.reload();
                return;
            }

            // Update download options
            downloadConfig.title = data.title;
            downloadConfig.videoUrl = data.url;
            downloadConfig.subtitleId = data.subtitleId;
            download_form.data('config', downloadConfig);

            // Change player
            if (!api) {
                try {
                    api = videojs('player');
                }
                catch (e) {
                }
            }

            if (api) {
                api.dispose();
            }

            $('.video-container').html(data.html);
            videojs('player');

            $('.bestResolution').text(data.bestResolution);

            // Enable download buttons
            buttons.prop('disabled', false);
            labels.removeClass('disabled');
        }).fail(function(jqXHR, textStatus, errorThrown) {
            if (message) {
                message.close();
            }

            if (api) {
                api.createModal('Видео клипа не е намерен. Моля опитайте отново по-късно.', {
                    unclosable: true
                });
            }
        });
    }

    var player = $('#player');
    if (player.length > 0) {
        videojs(player.get(0)).on('error', function() {
            console.log('recover videojs onerror');
            recoverPlayback(this);
        });
        $('.refresh').on('click', function() {
            $(this).remove();
            var playerApi;
            try {
                playerApi = videojs('player');
            }
            catch (e) {
                playerApi = undefined;
            }
            recoverPlayback(playerApi, true);
        });
    }

    // Detect iOS
    function iOSversion() {
        if (/iP(hone|od|ad)/.test(navigator.platform)) {
            // supports iOS 2.0 and later: <https://bit.ly/TJjs1V>
            var v = (navigator.appVersion).match(/OS (\d+)_(\d+)_?(\d+)?/);
            return [parseInt(v[1], 10), parseInt(v[2], 10), parseInt(v[3] || 0, 10)];
        }

        return [0, 0, 0];
    }

    var iOSver = iOSversion();

    if (iOSver[0] && iOSver[0] < 13) {
        $('.options-container').text('Вашето устройство (iPhone, iPad или iPod) няма възможност да сваля клипове и песни от Интернет, освен чрез Apple Music или iTunes. Свалянето на клипове и песни от нашия сайт е възможно само на устройства с iOS 13 или по-нова версия. Поддържат се следните устройства: iPhone 6S и по-нови, iPad Air 2 и по-нови, iPad mini 4 и iPhone SE. За съжаление, не възможно свалянето ако ползвате iPhone 5S, iPhone 6, iPhone 6 Plus, iPad Air, iPad mini 2, iPad mini 3 или по-стари iOS устройства.');
    }
});
