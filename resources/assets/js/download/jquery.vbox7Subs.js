(function($) {
	function leadingZero(num) {
		if (num < 10) {
			return '0' + num;
		}

		return num;
	}

	function getTime(seconds, sep) {
		if (!sep) {
			sep = '.';
		}
		
		var time = new Date(0);
		
		time.setUTCSeconds(seconds);
		
		return leadingZero(time.getUTCHours()) + ':' +
			leadingZero(time.getUTCMinutes()) + ':' +
			leadingZero(time.getUTCSeconds()) + sep + '000';
	}

	function pushLine(arr, text) {
		arr.push(text + '\n');
	}
	
	function getVbox7SubsUrl(id) {
		id = id.toString();
		var dir = id.substr(-3);
		dir = (new Array(3 - dir.length + 1).join('0')) + dir;
		return 'https://i49.vbox7.com/subtitles/' + dir + '/' + id + '_2.js';
	}
	
	function convertVbox7Subs(data, format) {
		if (!format) {
			format = 'vtt';
		}
		
		var subs = [];
		var sep = null;
		var type = 'text/plain';
		
		if (format === 'vtt') {
			sep = '.';
			pushLine(subs, 'WEBVTT');
		}
		
		if (format === 'srt') {
			sep = ',';
			subs.push('\uFEFF'); // byte order mark
		}
		
		var idx = 1;
		
		$.each(data, function() {
			if (!('f' in this) || !('t' in this) || !('s' in this)) {
				return;
			}
			
			var text = $.trim(this.s);
			
			if (text.length === 0) {
				return;
			}
			
			if (format === 'srt') {
				pushLine(subs, idx++);
			}
			
			pushLine(subs, getTime(this.f, sep) + ' --' + '> ' + getTime(this.t, sep));
			
			$.each(text.split('<br>'), function() {
				var line = this.toString();
				
				if (line.length === 0) {
					return;
				}
				
				pushLine(subs, line);
			});
			
			pushLine(subs, '');
		});

		return new Blob(subs, {
			type: 'text/' + format + '; charset=utf-8'
		});
	}

    function attachVbox7Subs(subs) {
		return this.append($('<track>').attr({
			kind: 'subtitles',
			'default': true,
			srclang: 'bg',
			label: 'Български',
			src: URL.createObjectURL(convertVbox7Subs(subs, 'vtt'))
		}));
    }

    function utf8_decode(str_data) {
        var tmp_arr = [],
            i = 0,
            ac = 0,
            c1 = 0,
            c2 = 0,
            c3 = 0,
            c4 = 0;

        str_data += '';

        while (i < str_data.length) {
            c1 = str_data.charCodeAt(i);

            if (c1 <= 191) {
                tmp_arr[ac++] = String.fromCharCode(c1);
                i++;
            } else if (c1 <= 223) {
                c2 = str_data.charCodeAt(i + 1);
                tmp_arr[ac++] = String.fromCharCode(((c1 & 31) << 6) | (c2 & 63));
                i += 2;
            } else if (c1 <= 239) {
                // http://en.wikipedia.org/wiki/UTF-8#Codepage_layout
                c2 = str_data.charCodeAt(i + 1);
                c3 = str_data.charCodeAt(i + 2);
                tmp_arr[ac++] = String.fromCharCode(((c1 & 15) << 12) | ((c2 & 63) << 6) | (c3 & 63));
                i += 3;
            } else {
                c2 = str_data.charCodeAt(i + 1);
                c3 = str_data.charCodeAt(i + 2);
                c4 = str_data.charCodeAt(i + 3);
                c1 = ((c1 & 7) << 18) | ((c2 & 63) << 12) | ((c3 & 63) << 6) | (c4 & 63);
                c1 -= 0x10000;
                tmp_arr[ac++] = String.fromCharCode(0xD800 | ((c1 >> 10) & 0x3FF));
                tmp_arr[ac++] = String.fromCharCode(0xDC00 | (c1 & 0x3FF));
                i += 4;
            }
        }

        return tmp_arr.join('');
    }

    function stripslashes(str) {
        return (str + '').replace(/\\(.?)/g, function (s, n1) {
            switch (n1) {
                case '\\':
                    return '\\';
                case '0':
                    return '\u0000';
                case '':
                    return '';
                default:
                    return n1;
            }
        });
    }

    function parseOriginalFile(data) {
        var jsData = utf8_decode(stripslashes(data));
        jsData = jsData.replace('var sSubsJson = \'', 'var sSubsJson=\'');

        if (jsData.indexOf('var sSubsJson=\'') !== 0 || jsData.lastIndexOf('\';') !== jsData.length - 2) {
        	return false;
        }

        return $.parseJSON(jsData.substr(15, jsData.length - 15 - 2));
    }

	// EXPORT
	
	$.vbox7Subs = {
		getUrl: getVbox7SubsUrl,
		convert: convertVbox7Subs,
        parseOriginal: parseOriginalFile
	};
	
	$.fn.attachVbox7Subs = attachVbox7Subs;
}(jQuery));
