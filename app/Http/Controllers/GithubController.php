<?php

namespace App\Http\Controllers;

use App;
use Dotenv\Loader;
use Illuminate\Http\Request;
use Response;

class GithubController extends Controller
{
    private static $commands = [
        'php artisan down',
        'git pull --ff-only',
        /*
        [
            'composer install --no-dev',
            'composer install',
        ],
        [
            'php artisan migrate --step --force',
            'php artisan migrate --step',
        ],
        'php artisan cache:clear',
        'php artisan view:clear',
        'npm install',
        'gulp --production',
        */
        'php artisan up',
    ];

    private $dotEnvLoader;

    public function __construct()
    {
        $this->dotEnvLoader = new Loader(base_path() . DIRECTORY_SEPARATOR . '.env');
    }

    private function prependToPath($directory)
    {
        if (!is_dir($directory)) {
            return false;
        }

        $path = $this->dotEnvLoader->getEnvironmentVariable('PATH');

        if (is_null($path)) {
            $path = '/usr/local/bin:/usr/bin:/bin';
        }

        if (strpos($path, $directory) === false) {
            $this->dotEnvLoader->setEnvironmentVariable('PATH', $directory . ':' . $path);
            return true;
        }

        return false;
    }

    private function modifyPath()
    {
        return $this->prependToPath(
            $this->dotEnvLoader->getEnvironmentVariable('HOME') . DIRECTORY_SEPARATOR . 'bin'
        );
    }

    private function loadNvm()
    {
        $nvmDir = $this->dotEnvLoader->getEnvironmentVariable('HOME') .
            DIRECTORY_SEPARATOR . '.nvm';
        $nvmLoader = $nvmDir . DIRECTORY_SEPARATOR . 'nvm.sh';

        if (
            !file_exists($nvmLoader) ||
            !is_null($this->dotEnvLoader->getEnvironmentVariable('NVM_DIR'))
        ) {
            return false;
        }

        $this->dotEnvLoader->setEnvironmentVariable('NVM_DIR', $nvmDir);
        $nvmBinary = shell_exec('. ' . $nvmLoader . ' && nvm which default');

        if (is_null($nvmBinary)) {
            return false;
        }

        $nvmBinary = rtrim($nvmBinary);
        $nvmBinaryDir = pathinfo($nvmBinary, PATHINFO_DIRNAME);

        return $this->prependToPath($nvmBinaryDir);
    }

    public function webhook(Request $request)
    {
        $signature = $request->header('X-Hub-Signature');

        if (is_null($signature) || strpos($signature, '=') === false) {
            return Response::make('Missing signature', 422);
        }

        list($algorithm, $hash) = explode('=', $signature);
        $content = $request->getContent();

        if (hash_hmac($algorithm, $content, env('GITHUB_SECRET')) !== $hash) {
            return Response::make('Incorrect signature', 403);
        }

        if (!in_array($request->header('X-GitHub-Event'), ['push', 'ping'])) {
            return Response::make('No action set for specified event', 202);
        }

        $json = $request->json();

        if ($json->get('ref') !== 'refs/heads/master') {
            return Response::make('Ignoring push on non-master branch', 202);
        }

        $response = 'Performing deploy...' . "\n\n";

        $basePath = base_path();
        $response .= 'Changing directory to: ' . $basePath . "\n\n";
        chdir($basePath);

        /*
        if ($this->modifyPath()) {
            $response .= 'Prepended local bin directory to PATH' . "\n\n";
        }

        if ($this->loadNvm()) {
            $response .= 'Loaded NVM' . "\n\n";
        }
        */

        foreach (static::$commands as $command) {
            if (is_array($command)) {
                $command = $command[intval(App::isLocal())];
            }

            if (is_null($command) || strlen($command) === 0) {
                continue;
            }

            $response .= '$ ' . $command . "\n";
            $response .= shell_exec($command . ' 2>&1') . "\n";
        }

        return Response::make($response);
    }
}
