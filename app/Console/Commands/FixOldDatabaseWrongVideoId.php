<?php

namespace App\Console\Commands;

use App\Helpers\MassMysqlHelper;
use DB;
use Illuminate\Console\Command;

class FixOldDatabaseWrongVideoId extends Command
{
    protected $signature = 'vbox7mp3:fix_old_wrong_vid_id';
    protected $description = 'Fix wrong video_id in old database';

    public function handle()
    {
        $chunkSize = env('CHUNK_SIZE_UPDATE', 1000);
        $mediaUrlRegex = '#^(?:https?:)?//media(?P<server_id>\d+)?\.vbox7\.com/s/[a-f0-9]{2}/' .
            '(?P<video_id>[a-f0-9]{8,10})(?:r(?P<video_key>[a-f0-9]{9}))?\.(?P<video_type>flv|mp4)$#';

        $oldConn = DB::connection('old_mysql');

        $count = $oldConn->table('videos')->count();
        $chunks = intdiv($count, $chunkSize) + 1;
        $barSteps = $chunks + 1;

        $bar = $this->output->createProgressBar($barSteps);
        $bar->setRedrawFrequency($barSteps / 100);

        $bar->advance();
        $bar->display();

        $oldConn->table('videos')->select(['id', 'video_url'])->orderBy('id')
            ->chunk($chunkSize, function($rows) use ($mediaUrlRegex, $oldConn, $bar) {
                $data = [];

                foreach ($rows as $row) {
                    if (!preg_match($mediaUrlRegex, $row->video_url, $matches)) {
                        throw new \Exception('Failed to parse URL: ' . $row->video_url);
                    }

                    $data[$row->id] = [
                        'server_id' => (strlen($matches['server_id']) > 0) ? intval($matches['server_id']) : null,
                        'video_id' => hex2bin($matches['video_id']),
                        'video_key' => (strlen($matches['video_key']) > 0) ? hex2bin('0' . $matches['video_key']) :
                            null,
                        'video_type' => [
                            'flv' => 1,
                            'mp4' => 2,
                        ][$matches['video_type']],
                    ];
                }

                $oldConn->update(...MassMysqlHelper::generateUpdate('videos', $data));
                $bar->advance();
            });

        $bar->finish();
    }
}
