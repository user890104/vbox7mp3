<?php
namespace App\Http\Middleware;

use Closure;

class SetExpire {

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param \Closure|Closure $next
     * @param int $expire
     * @return mixed
     * @internal param null|string $guard
     */
    public function handle($request, Closure $next, $expire = 0)
    {
		$response = $next($request);

		if (config('app.env') !== 'production') {
			return $response;
		}

		return $response->setMaxAge($expire);
    }
}
