<?php

namespace App\Http\ViewComposers;

use App\Video;
use Illuminate\View\View;

class ChosenVideosComposer
{
    public function compose(View $view)
    {
        $view->with('videos', Video::getChosen());
    }
}
