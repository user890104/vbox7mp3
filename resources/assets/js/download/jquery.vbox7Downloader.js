(function($) {
    var stderrBuf = [];
    var progressAudioScript = 0;
    var progressAudioVideo = 0;
    var progressAudioConvert = 0;

    function progressHandler(callback, opts) {
        if (!opts) {
            opts = {};
        }

        return function(e) {
            var totalSize;

            if (e.lengthComputable) {
                totalSize = e.total;
            }
            else {
                if (opts.totalSize) {
                    totalSize = opts.totalSize;
                }
                else {
                    return;
                }
            }

            callback(e.loaded / totalSize);
        };
    }

    function createWorker(workerUrl) {
        return new Promise(function(resolve, reject) {
            var worker = new Worker(workerUrl);

			if (!worker) {
				reject(new Error('Worker initialisation failed!'));
			}
			
            worker.addEventListener('message', function(e) {
                switch (e.data.type) {
                    case 'ready':
                        console.info('worker finished loading');
                        resolve(worker);
                        break;
                    case 'start':
                        console.info('processing started');
                        break;
                    case 'exit':
                        console.info('exit code', e.data.data);
                        break;
                    case 'done':
                        console.info('processing finished in', e.data.time / 1000, 's');
                        break;
                    case 'fail':
                        console.info('processing failed');
                        break;
                    case 'stdout':
                        console.log(e.data.data);
                        break;
                    case 'stderr':
                        (console.debug || console.log).bind(console)(e.data.data);
                        stderrBuf.push(e.data.data);
                        break;
                    default:
                        console.warn('unknown message type', e.data.type);
                        console.log('message', e);
                        break;
                }
            }, false);
        });
    }

    function convertTime(matches) {
        return parseInt(matches[1]) * 60 * 60 + parseInt(matches[2]) * 60 + parseFloat(matches[3]);
    }

    function setProgress(sheet, type, value) {
        if (type === null) {
            sheet.deleteRule(0);
            return;
        }

        var cssText = '.download.' + type + ':after { width: ' + value * 100 + '%; }';
        sheet.insertRule(cssText, 0);

        while (sheet.cssRules.length > 1) {
            sheet.deleteRule(1);
        }
    }

    function setAudioProgress(sheet) {
        var state = progressAudioScript * .04 + progressAudioVideo * .24 + progressAudioConvert * .72;
        setProgress(sheet, 'audio', state);
    }

    function download(opts) {
        if (
            !('id' in opts) ||
            !('subtitleId' in opts) ||
            !('type' in opts) ||
            !('title' in opts) ||
            !('videoUrl' in opts)
        ) {
            return Promise.reject(new Error('Missing general parameters'));
        }

        var promiseChain;
        var filename;
        var progressHack = $('#progress-hack').get(0).sheet;

        if (['video', 'audio'].indexOf(opts.type) > -1) {
            var trackUrl = '/track?type=' + opts.type + '&video_id=' + opts.id;

            if (!('sendBeacon' in navigator) || !navigator.sendBeacon(trackUrl)) {
                fetch(trackUrl);
            }
        }

        switch (opts.type) {
            case 'video':
                filename = opts.title + '.mp4';

                promiseChain = $.forceDownload({
                    url: opts.videoUrl,
                    filename: filename,
                    onProgress: progressHandler(function(state) {
                        setProgress(progressHack, opts.type, state);
                    }),
                    onChromeWarning: function(filename) {
                        //
                    },
                    linkContainer: null,
                    linkText: 'Натиснете ТУК за сваляне'
                }).lastly(function() {
                    setProgress(progressHack);
                });
                break;
            case 'audio':
                if (
                    !('scriptUrl' in opts) ||
                    !('scriptSize' in opts)
                ) {
                    return Promise.reject(new Error('Missing audio parameters'));
                }

                if (!$.canSaveBlob()) {
                    promiseChain = Promise.reject(new Error('No supported download methods detected'));
                    break;
                }

				if (!('Worker' in window)) {
					promiseChain = Promise.reject(new Error('Web Workers not available'));
					break;
				}

                progressAudioScript = progressAudioVideo = progressAudioConvert = 0;
                filename = opts.title + '.mp3';
                var regex_time = '(\\d{2}):(\\d{2}):(\\d{2}\\.\\d{2})';
                var regex_total = new RegExp('  Duration: ' + regex_time + ', start: ');
                var regex_converted = new RegExp(' time=' + regex_time + ' bitrate=');

                var promiseScript = $.xhrFetch({
                    url: opts.scriptUrl,
                    onProgress: progressHandler(function(state) {
                        if (state < progressAudioScript) {
                            return;
                        }

                        progressAudioScript = state;
                        setAudioProgress(progressHack);
                    }, {
                        totalSize: opts.scriptSize
                    })
                }).then(function(response) {
                    var workerUrl = URL.createObjectURL(response);
                    return createWorker(workerUrl);
                });

                var promiseVideo = $.xhrFetch({
                    url: opts.videoUrl,
                    onProgress: progressHandler(function(state) {
                        progressAudioVideo = state;
                        setAudioProgress(progressHack);
                    })
                });

                promiseChain = Promise.all([promiseScript, promiseVideo]).then(function(result) {
                    return new Promise(function(resolve, reject) {
                        var worker = result[0];
                        var videoFile = result[1];

                        var totalTime = null;
                        var elapsedTime = null;

                        function messageHandler(e) {
                            switch (e.data.type) {
                                case 'stderr':
                                    var time = e.data.data.match(regex_total);

                                    if (time && time.length === 4) {
                                        totalTime = convertTime(time);
                                    }

                                    time = e.data.data.match(regex_converted);

                                    if (time && time.length === 4) {
                                        elapsedTime = convertTime(time);
                                    }

                                    if (totalTime !== null && elapsedTime !== null) {
                                        progressAudioConvert = elapsedTime / totalTime;
                                        setAudioProgress(progressHack);
                                    }
                                    break;
                                case 'exit':
                                    if (e.data.data === 0) {
                                        break;
                                    }

                                    worker.removeEventListener('message', messageHandler, false);
                                    worker.removeEventListener('error', errorHandler, false);
                                    reject(new Error('Process exited with code ' + e.data.data));
                                    break;
                                case 'done':
                                    worker.removeEventListener('message', messageHandler, false);
                                    worker.removeEventListener('error', errorHandler, false);
                                    resolve(e.data);
                                    break;
                            }
                        }

                        function errorHandler(e) {
                            worker.removeEventListener('message', messageHandler, false);
                            worker.removeEventListener('error', errorHandler, false);
                            reject(e);
                        }

                        worker.addEventListener('message', messageHandler, false);
                        worker.addEventListener('error', errorHandler, false);

                        var inputFilename = 'input.mp4';
                        var inputPath = '/blob/' + inputFilename;

                        var outputPath = '/tmp/output.mp3';

                        worker.postMessage({
                            type: 'run',
                            arguments: [
                                '-i',
                                inputPath,
                                '-vn',
                                '-acodec',
                                'libmp3lame',
                                '-ab',
                                '128k',
                                outputPath
                            ],
                            inputFiles: [{
                                name: inputFilename,
                                data: videoFile
                            }],
                            outputFiles: [{
                                name: outputPath,
                                type: 'audio/mpeg'
                            }]
                        });
                    });
                }).then(function(data) {
                    if (data.data.length === 1) {
                        return $.saveBlob(data.data[0], {
                            filename: filename,
                            linkContainer: null,
                            linkText: 'Натиснете ТУК за сваляне'
                        }).then(function() {
                            return data.time;
                        });
                    }

                    throw new Error('Received wrong number of files (' + data.data.length + ')');
                }).lastly(function() {
                    setProgress(progressHack);
                });
                break;
            case 'subs':
                if (!$.canSaveBlob()) {
                    promiseChain = Promise.reject(new Error('No supported download methods detected'));
                    break;
                }

                filename = (opts.title || 'subtitles') + '.srt';

                promiseChain = $.xhrFetch({
                    url: $.vbox7Subs.getUrl(opts.subtitleId),
                    responseType: 'text',
                    onProgress: progressHandler(function(state) {
                        setProgress(progressHack, opts.type, state);
                    })
                }).then(function(data) {
                    var subs = $.vbox7Subs.parseOriginal(data);

                    if (!subs) {
                        return Promise.reject(new Error('Invalid subtitle file'));
                    }

                    var blob = $.vbox7Subs.convert(subs, 'srt');

                    return $.saveBlob(blob, {
                        filename: filename,
                        linkContainer: null,
                        linkText: 'Натиснете ТУК за сваляне'
                    });
                }).lastly(function() {
                    setProgress(progressHack);
                });
                break;
        }

        if (!promiseChain) {
            return Promise.reject(new Error('Unknown download type'));
        }

        return promiseChain.then(function(data) {
            if ('ga' in window) {
                ga('send', 'event', 'downloadSuccess', opts.type, opts.videoUrl, data, {
                    transport: 'beacon'
                });
            }
        }).error(function() {
            alert('Свалянето беше прекъснато. Моля опитайте отново.');
        }).caught(function(e) {
            var isExpected = (
				e.message === 'No supported download methods' ||
				e.message === 'No supported download methods detected' ||
				e.message === 'FileReaderSync not available' ||
				e.message === 'FileReaderSync is not defined' ||
				e.message === 'Web Workers not available' ||
				e.message === 'msSaveBlob not implemented'
            );

            if ('ga' in window) {
                ga('send', 'event', isExpected ? 'downloadLegacy' : 'downloadFailed', opts.type, opts.videoUrl, {
                    transport: 'beacon'
                });
            }

            if (isExpected) {
                alert('Не е възможно свалянето на този клип в момента. Моля опитайте отново по-късно.');
                return;
            }

            throw e;
        });
    }

    $.vbox7Download = download;
})(jQuery);
