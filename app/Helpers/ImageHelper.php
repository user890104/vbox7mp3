<?php
/**
 * Created by PhpStorm.
 * User: Venci
 * Date: 2.12.2018 г.
 * Time: 19:57 ч.
 */

namespace App\Helpers;


class ImageHelper
{
    public static function getJPEGImageXY($data)
    {
        $soi = unpack('nmagic/nmarker', $data);

        if ($soi['magic'] !== 0xFFD8) {
            return false;
        }

        $marker = $soi['marker'];
        $data   = substr($data, 4);

        while (true) {
            if (strlen($data) === 0) {
                return false;
            }

            switch($marker) {
                case 0xFFC0:
                    $info = unpack('nlength/Cprecision/nY/nX', $data);
                    return $info;
                    break;

                default:
                    $info = unpack('nlength', $data);
                    $data = substr($data, $info['length']);
                    $info = unpack('nmarker', $data);
                    $marker = $info['marker'];
                    $data = substr($data, 2);
                    break;
            }
        }

        return false;
    }
}
