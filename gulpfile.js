'use strict';

const fs = require('fs');

const gulp = require('gulp');
const gzip = require('gulp-gzip');

const elixir = require('laravel-elixir');
const Task = elixir.Task;

const sharp = require('sharp');

const asyncForEach = async (array, callback) => {
    for (let index = 0; index < array.length; index++) {
        await callback(array[index], index, array);
    }
};

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for your application as well as publishing vendor resources.
 |
 */

elixir.config.versioning.buildFolder = 'assets';

elixir.extend('gzip', function(src, dest, opts) {
	opts = opts || {};

	new Task('gzip', function() {
		return gulp
			.src(src)
			.pipe(gzip(opts))
			.pipe(gulp.dest(dest));
	});
});

elixir.extend('icons', function(src, dest, opts) {
	opts = opts || {};

	new Task('icons', function() {
		return new Promise(function(resolve) {
			fs.mkdir(dest, function() {
				resolve(dest);
			});
		}).then(function(dir) {
            return asyncForEach(opts.sizes, function(size) {
            	const sizeText = size + 'x' + size;
                console.log('Generating icon ' + sizeText);

                return sharp(src)
                    .resize(size, size)
                    .toFile(dir + '/' + size + '.png');
            });
		});
    });
});

elixir.extend('splashScreens', function(src, dest, opts) {
	opts = opts || {};

	new Task('splashScreens', function() {
		return new Promise(function(resolve) {
			fs.mkdir(dest, function() {
				resolve(dest);
			});
		}).then(function(dir) {
            return asyncForEach(opts.sizes, function(size) {
            	const sizeText = size[0] + 'x' + size[1];
                console.log('Generating splash screen ' + sizeText);

                return sharp(src)
                    .resize(size[0], size[1], {
                    	fit: 'contain',
                    	background: {
                    		r: 0,
                    		g: 0,
                    		b: 0,
                    		alpha: 0
                    	}
                    })
                    .toFile(dir + '/' + sizeText + '.png');
            });
		});
    });
});

elixir(function(mix) {
	mix
        .sass('vbox7mp3.scss')
		.styles([
			'../../../node_modules/video.js/dist/video-js.css',
			'../../../public/css/vbox7mp3.css'
		], 'public/css/app.css')
		.scripts([
			'../../../node_modules/jquery/dist/jquery.js',
            '../../../node_modules/bluebird/js/browser/bluebird.core.js',
			'../../../node_modules/speakingurl/lib/speakingurl.js',
			'../../../node_modules/video.js/dist/video.js',
			'../../../node_modules/video.js/dist/lang/bg.js',
            'lib/videojs-media-session.js',
			'download/*.js',
			'app.js'
		], 'public/js/app.js')
        .copy('resources/assets/img', 'public/img')
        .copy('resources/assets/js/sw.js', 'public')
	    .copy('resources/assets/js/download/ffmpeg', 'public/js/ffmpeg')
		.icons('resources/assets/img/icon.png', 'public/img/icons', {
            sizes: [
            	57, 72, 76, 144, 152, 180, // link
            	36, 48, /* 72, */ 96, /* 144, */ 192, 256, 384, 512 // manifest
			]
		})
		.splashScreens('resources/assets/img/logo-2000.png', 'public/img/splash', {
            sizes: [ // link
                [320, 460],
                [640, 920],
                [640, 1096],
                [748, 1024],
                [750, 1294],
                [768, 1004],
                [1182, 2208],
                [1242, 2148],
                [1496, 2048],
                [1536, 2008],
            ],
		})
        .version([
			'css/app.css',
            'js/app.js',
            'js/ffmpeg/ffmpeg.js',
            'img'
		])
        .gzip('public/sw.js', 'public')
		.gzip('public/assets/css/app-*.css', 'public/assets/css')
		.gzip('public/assets/js/app-*.js', 'public/assets/js')
        .gzip('public/assets/js/ffmpeg/ffmpeg-*.js', 'public/assets/js/ffmpeg');
});
