<noscript class="lazy-image content">
    {!! Html::image($url, $alt, $attributes) !!}
</noscript>
