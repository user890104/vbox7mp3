@extends('layouts.app.text')

@section('text_title')
Търсене
@if(request()->has('q'))
на "{{ request()->input('q') }}"
@endif
@endsection

@section('text_content')
    @if(!Request::has('q'))
    <p>Въведете ключова дума (име на песен или изпълнител) в полето по-долу.</p>
    @endif
    <div>
        <gcse:searchbox-only></gcse:searchbox-only>
    </div>
    <div>
        <gcse:searchresults-only></gcse:searchresults-only>
    </div>
@endsection

@section('footer')
    <script type="text/javascript">
        (function() {
            var cx = '013982944046502105560:WMX-419078065';
            var gcse = document.createElement('script');
            gcse.type = 'text/javascript';
            gcse.async = true;
            gcse.src = 'https://www.google.com/cse/cse.js?cx=' + cx;
            var s = document.getElementsByTagName('script')[0];
            s.parentNode.insertBefore(gcse, s);
        })();
    </script>
@endsection
