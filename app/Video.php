<?php

namespace App;

use App\Helpers\Exception\videoDownloaderException\temporaryException\downloadException;
use App\Helpers\VideoDownloader;
use Base64Url\Base64Url;
use Cache;
use DB;
use Eloquent;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use LogicException;
use Psr\SimpleCache\InvalidArgumentException;

/**
 * App\Video
 *
 * @property mixed id
 * @property mixed server_id
 * @property mixed secure_hash
 * @property mixed secure_ts
 * @property mixed video_id
 * @property mixed video_key
 * @property mixed resolutions
 * @property mixed filetype
 * @property mixed title
 * @property mixed uploader
 * @property-read mixed $count_downloads_today
 * @property-read mixed $page_url
 * @property-read mixed $picture0
 * @property-read mixed $picture1
 * @property-read mixed $picture2
 * @property-read mixed $url
 * @property-read Collection|Log[] $logs
 * @property-read int best_resolution
 * @property-read int|null subtitle_id
 * @mixin Eloquent
 */
class Video extends Model
{
    public $fillable = [
        'server_id',
        'secure_hash',
        'secure_ts',
        'video_id',
        'video_key',
        'resolutions',
        'filetype',
        'duration',
        'subtitle_id',
        'title',
        'uploader',
        'image_width',
        'image_height',
    ];

    public $timestamps = false;

    const RESOLUTIONS = [
        0 => null,
        1 => 144,
        2 => 240,
        3 => 480,
        4 => 720,
        5 => 1080,
        6 => 2160,
    ];

    public function logs()
    {
        return $this->hasMany('App\Log');
    }

    public function getSecureHashAttribute($value)
    {
        return Base64Url::encode($value);
    }

    public function setSecureHashAttribute($value)
    {
        $this->attributes['secure_hash'] = Base64Url::decode($value);
    }

    public function getVideoIdAttribute($value)
    {
        return bin2hex($value);
    }

    public function setVideoIdAttribute($value)
    {
        $this->attributes['video_id'] = hex2bin($value);
    }

    public function getVideoKeyAttribute($value)
    {
        return is_null($value) ? null : substr(bin2hex($value), 1);
    }

    public function setVideoKeyAttribute($value)
    {
        $this->attributes['video_key'] = is_null($value) ? null : hex2bin('0' . $value);
    }

    public function getCountDownloadsTodayAttribute()
    {
        if (isset($this->logs[0])) {
            return $this->logs[0]->count_video + $this->logs[0]->count_audio;
        }

        return 0;
    }

    public function getCountViewsTodayAttribute()
    {
        if (isset($this->logs[0])) {
            return $this->logs[0]->count_watch;
        }

        return 0;
    }

    public static function generatePictureUrl($videoId, $size = 0)
    {
        return 'https://i49.vbox7.com/' . ($size === 0 ? 'o' : 'i') .
            '/' . substr($videoId, 0, 3) . '/' . $videoId . $size . '.jpg';
    }

    private function getPictureUrl($size = 0)
    {
        return static::generatePictureUrl($this->video_id, $size);
    }

    public function getPicture0Attribute()
    {
        return $this->getPictureUrl(0);
    }

    public function getPicture1Attribute()
    {
        return $this->getPictureUrl(1);
    }

    public function getPicture2Attribute()
    {
        return $this->getPictureUrl(2);
    }

    private function getVideoHost()
    {
        if (is_null($this->server_id)) {
            $host = 'media';
        }
        else {
            //$host = 'edge' . rand(1, 3) . str_pad($this->server_id, 2, '0', STR_PAD_LEFT);
            $host = 'media' . str_pad($this->server_id, 2, '0', STR_PAD_LEFT);
        }

        return $host . '.vbox7.com';
    }

    private function formatVideoUrl($suffix = '')
    {
        $secureLink = !is_null($this->secure_ts) && !is_null($this->secure_hash);
        $hasKey = !is_null($this->video_key);

        return 'https://' . $this->getVideoHost() . '/s' .
            ($secureLink ? ('l/' . $this->secure_hash . '/' . $this->secure_ts) : '') .
            '/' . substr($this->video_id, 0, 2) . '/' .
            ($hasKey ? '' : ($this->video_id . '/')) . $this->video_id .
            ($hasKey ? ('r' . $this->video_key) : '') . $suffix . '.' . $this->filetype;
    }

    public function getBestResolutionAttribute()
	{
		return $this->getResolution(0);
	}

	private function getResolution($resolution)
	{
        if (is_null($this->video_key) && $this->resolutions & 0b01111110) {
            if ($resolution === 0) {
                for ($i = 6; $i > 0; --$i) {
                    if (($this->resolutions >> $i) > 0) {
                        $resolution = $i;
                        break;
                    }
                }
            }

            if (!is_null(static::RESOLUTIONS[$resolution])) {
                return static::RESOLUTIONS[$resolution];
            }
        }

        return 0;
	}

    private function getVideoResolutionSuffix($resolution)
    {
        $resolution = $this->getResolution($resolution);

        if ($resolution) {
        	return '_' . strval($resolution);
		}

        return '';
    }

    private function getVideoUrl($resolution = 0)
    {
        return $this->formatVideoUrl($this->getVideoResolutionSuffix($resolution));
    }

    public function getUrlAttribute()
    {
        return $this->getVideoUrl();
    }

    public function getPageUrlAttribute()
    {
        return route('watch', [
            'video' => $this->video_id,
            'suffix' => '-' . rawurlencode(str_replace([
                '/',
                '{',
                '}',
            ], [
                '-',
                '[',
                ']',
            ], $this->title)) . '.html',
		]);
    }

    public function getSubtitleUrlAttribute()
    {
        if (is_null($this->subtitleId)) {
            return null;
        }

        $dir = substr($this->subtitleId, -3);
        $dir = str_pad($dir, 3, '0', STR_PAD_LEFT);
		return 'https://i49.vbox7.com/subtitles/' . $dir . '/' . $this->subtitleId . '_2.js';
    }

    public static function getChosen()
    {
        return [];
        return Cache::remember('videos._chosen', 30, function () {
            return static::findMany([
				3561049,
				3566240,
                3568304,
				3571433,
				3573545,
				3577036,
				3578574,
				3580431,
                3580979,
				3573713,
				3582445,
				3584135,
            ]);
        });
    }

    public static function getMostDownloaded()
    {
        return Cache::remember('videos._mostDownloaded', 10, function () {
            return Log::with('video')
                ->whereRaw('logs.date = CURDATE()')
                ->where(DB::raw('count_video + count_audio'), '>', 0)
                ->orderBy(DB::raw('count_video + count_audio'), 'desc')
                ->take(12)
                ->get()
                ->sortByDesc(function($log, $key) {
                    return $log->count_video + $log->count_audio;
                })
                ->map(function(Log $log) {
                    $log->video->relations = [
                        'logs' => [
                            $log,
                        ],
                    ];
                    return $log->video;
                });
        });
    }

    public static function getMostWatched()
    {
        return Cache::remember('videos._mostWatched', 10, function () {
            return Log::with('video')
                ->whereRaw('logs.date = CURDATE()')
                ->where('count_watch', '>', 0)
                ->orderBy('count_watch', 'desc')
                ->take(12)
                ->get()
                ->sortByDesc('count_watch')
                ->map(function(Log $log) {
                    $log->video->relations = [
                        'logs' => [
                            $log,
                        ],
                    ];
                    return $log->video;
                });
        });
    }

    public static function fromVbox7Id($videoId, $download = false)
    {
        $cacheKey = 'videos.' . $videoId;
        $model = Cache::get($cacheKey);

        if (!is_null($model)) {
            return $model;
        }

        $collection = static::where('video_id', hex2bin($videoId))->get();

        if ($collection->isNotEmpty()) {
            $model = $collection->first();
            Cache::put($cacheKey, $model, 60);
            return $model;
        }

        if ($download) {
            $data = VideoDownloader::getData($videoId);
            $model = new static($data);
            $model->save();
            Cache::put($cacheKey, $model, 60);
            return $model;
        }

        throw new LogicException(VideoDownloader::getErrorMessage(100));
    }

    public function deleteFromCache()
    {
        try {
            Cache::delete('videos.' . $this->video_id);
        } catch (InvalidArgumentException $e) {
        }
    }

    public function deleteFromFastCGICache()
    {
        $client = new Client;

        try {
            $client->request('PURGE', $this->page_url);
        }
        catch (GuzzleException $e) {
        }
    }

    public function updateData($force = false)
    {
        if (!$force && VideoDownloader::validateUrl($this->url)) {
            return true;
        }

        try {
            $data = VideoDownloader::getData($this->video_id);

            if ($data['video_id'] !== $this->video_id) {
                $this->deleteFromCache();
                $this->deleteFromFastCGICache();
            }

            $this->video_id = $data['video_id'];

            // Update DB
            $this->update($data);

            // Update cache
            Cache::put('videos.' . $this->video_id, $this, 60);
        }
        catch (downloadException $e) {
            $this->delete();
            $this->deleteFromCache();
        }

        $this->deleteFromFastCGICache();

        return $this->exists;
    }
}
