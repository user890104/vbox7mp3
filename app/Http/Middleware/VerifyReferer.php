<?php
namespace App\Http\Middleware;

use Closure;
use Illuminate\Foundation\Application;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

class VerifyReferer {
    /**
     * The application instance.
     *
     * @var \Illuminate\Foundation\Application
     */
    protected $app;

    /**
     * The URIs that should be excluded from CSRF verification.
     *
     * @var array
     */
    protected $except = [
        'callbacks/github',
    ];

    /**
     * Create a new middleware instance.
     *
     * @param  \Illuminate\Foundation\Application $app
     */
    public function __construct(Application $app)
    {
        $this->app = $app;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     *
     * @throws \Illuminate\Session\TokenMismatchException
     */
    public function handle($request, Closure $next)
    {
        if (
            $this->isReading($request) ||
            $this->runningUnitTests() ||
            $this->shouldPassThrough($request) ||
            $this->refererMatch($request)
        ) {
            return $next($request);
        }

        throw new BadRequestHttpException('Invalid referer');
    }

    /**
     * Determine if the request has a URI that should pass through CSRF verification.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return bool
     */
    protected function shouldPassThrough($request)
    {
        foreach ($this->except as $except) {
            if ($except !== '/') {
                $except = trim($except, '/');
            }

            if ($request->is($except)) {
                return true;
            }
        }

        return false;
    }

    /**
     * Determine if the application is running unit tests.
     *
     * @return bool
     */
    protected function runningUnitTests()
    {
        return $this->app->runningInConsole() && $this->app->runningUnitTests();
    }

    /**
     * Determine if the referrer is allowed.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return bool
     */
    protected function refererMatch($request)
    {
        $referer = $request->header('Referer');

        if (is_null($referer)) {
            return false;
        }

        $origin = $request->getSchemeAndHttpHost() . $request->getBaseUrl();

        return stripos($referer, $origin) === 0;
    }

    /**
     * Determine if the HTTP request uses a ‘read’ verb.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return bool
     */
    protected function isReading($request)
    {
        return in_array($request->method(), ['HEAD', 'GET', 'OPTIONS']);
    }
}
